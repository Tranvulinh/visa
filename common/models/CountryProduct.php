<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
// use common\models\Common;
use yii\behaviors\SluggableBehavior;
/**
 */
class CountryProduct extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%country_product}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('country_product', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /* 
    * Danh sách tất cả các nước 
    * [BACKEND INDEX] [DRAG-DROP] [PHP]
    */
    public static function getAllCountryProduct(){
        $result = (new Query)
                ->select('id,name,image,status,time_create,positions')
                ->from(static::tableName())
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('country_product', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
    * Kiểm tra trùng title 
    **/
    public static function checkUniqueName($title,$id = null) {
         $result = (new Query)
                ->select('p.title')
                ->from('country_product p')
                ->where(['p.title' => $title])
                ->andWhere(['<>','p.id' , $id])
                ->one();
    return $result;
    }
    /**
     * Save form country product
     * @param $dataForm [dữ liệu từ form country product gửi lên server]
     */
    public static function updateCountryProduct($dataForm) {
        if(!empty($dataForm['id'])){ // nếu là update product
            $model = CountryProduct::findOne($dataForm['id']);
        }
        else { // nếu là thêm mới product
            $model = new CountryProduct();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
            $model->positions = CountryProduct::find()->max('id') + 1;
        }
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->title = ! empty($dataForm['title']) ? $dataForm['title'] : "" ;
        $model->description_vi = ! empty($dataForm['description_vi']) ? $dataForm['description_vi'] : "" ;
        $model->description_en = ! empty($dataForm['description_en']) ? $dataForm['description_en'] : "" ;
        $model->id_sub_type_product = ! empty($dataForm['subTypeProduct']) ? $dataForm['subTypeProduct'] : 0;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
    public static function getCountryProductId($id) {
        $result = (new Query)
                ->select('cp.name,cp.description_vi,cp.description_en,cp.id_sub_type_product,cp.title_url,tp.title_url as `tp_title_url`')
                ->from('country_product cp')
                ->leftJoin('sub_type_product tp', 'cp.id_sub_type_product = tp.id')
                ->where(['cp.status' => '1','cp.title_url' => $id])
                // ->where(['position' => '1'])
                ->one();
        return $result;

    }
    public static function getCountryProduct($id) {
        $result = (new Query)
                ->select('id,name')
                ->from(static::tableName())
                ->where(['id_sub_type_product' => $id])
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    /**
    Danh sách tất cả các nước
    [Sub-type-product-controller SubTypeProduct list-flag]
    **/
    public static function getCountryProductLink($id){
        $result = (new Query)
            ->select('ct.id,ct.name,ct.image,ct.title_url,st.title_url as `st_title_url`,tp.title_url as `tp_title_url`')
            ->from('country_product ct')
            ->leftJoin('sub_type_product st', 'ct.id_sub_type_product = st.id')
            ->leftJoin('type_product tp', 'st.id_type_product = tp.id')
            ->where(['st.title_url' => $id])
            ->andWhere(['ct.status' => 1])
            // ->orderBy('ct.positions desc')
            ->orderBy('ct.positions desc,ct.id desc')
            ->all();
        return $result;
    }
    public static function getCountryProductProduct($id) {
        $result = (new Query)
                ->select('p.title_url,p.id,p.name,p.description,p.image,p.id_type_product,tp.id as tp_id,tp.name as tp_name,tp.id as tp_id,p.short_description ,t.name as `t_name` , t.id as `t_id`, tp.title_url as `st_title_url`')
                ->from('product p')
                ->leftJoin('country_product ct', 'p.id_country_product = ct.id')

                ->leftJoin('sub_type_product tp', 'p.id_sub_type_product = tp.id')
                ->leftJoin('type_product t','p.id_type_product = t.id')
                ->where(['p.status' => '1','ct.title_url' => $id])
                ->orderBy('ct.positions desc,ct.id desc')
                // ->where(['position' => '1'])
                ->all();
        return $result;

    }
//  public static function getSubTypeProductFollowTypeProduct($id) {
    // public static function getProduct($id){
    // $result = (new Query)
    //         ->select('p.title_url,p.id,p.name,p.description,p.image,p.id_type_product,tp.id as tp_id,tp.name as tp_name,tp.id as tp_id,p.short_description ,t.name as `t_name` , t.id as `t_id`, tp.title_url as `s_title_url`')
    //         ->from('product p')
    //         ->leftJoin('sub_type_product tp', 'p.id_sub_type_product = tp.id')
    //         ->leftJoin('type_product t','p.id_type_product = t.id')
    //         ->where(['p.status' => '1','tp.title_url' => $id])
    //         // ->where(['position' => '1'])
    //         ->all();
    // return $result;


    // }
    /**
    Lấy tất cả tab
    **/
    public static function getTab($id) {
        $result = (new Query)
            ->select('ct.title_url,st.title_url as `st_title_url`,tp.title_url as `tp_title_url`,ct.description_vi,ct.description_en,tp.name as `tp_name`,st.name as `st_name`')
            ->from('country_product ct')
            ->leftJoin('sub_type_product st', 'ct.id_sub_type_product = st.id')
            ->leftJoin('type_product tp', 'st.id_type_product = tp.id')
            ->where(['ct.title_url' => $id])
            // ->andWhere(['ct.status' => 1])
            ->one();
        return $result;
    }
    public static function deleteCountryProduct($id){
        $model = CountryProduct::findOne($id);
        return $model->delete();
    }
}