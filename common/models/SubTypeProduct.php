<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use common\models\SubTypeProduct;
use yii\behaviors\SluggableBehavior;
use yii\db\Expression;


class SubTypeProduct extends ActiveRecord 
{
  
    public static function tableName()
    {
        return '{{%sub_type_product}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('sub_type_product', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('sub_type_product', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
    * Kiểm tra trùng title 
    * [BACKEND ADD - UPDATE] 
    * [CONTROLLER ADD - UPDATE] [PHP]
    **/
    public static function checkUniqueName($title,$id = null) {
         $result = (new Query)
                ->select('p.title')
                ->from('sub_type_product p')
                ->where(['p.title' => $title])
                ->andWhere(['<>','p.id' , $id])
                ->one();
    return $result;
    }
    /* 
    * Danh sách tất cả visa
    * [BACKEND INDEX]
    */
    public static function getAllSubTypeProduct() {
        $result = (new Query)
                ->select('id,name,description,image,time_create,status,positions')
                ->from(static::tableName())
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
        
    }
    public static function getAllSubTypeProduct1() {
        // public function getAllSlider(){
            $result = (new Query)
                    ->select('id,name,description,image,time_create,status')
                    ->from(static::tableName())
                    ->where(['id_type_product' => 13])
                    ->orderBy('positions desc,id desc')
                    ->all();
            return $result;
        // }
    }
    /**
    * Lấy visa trên menu
    * [FRONTEND] [CONTROLLER CHUNG]
    **/
    public static function subTypeProductMenu() {
        $result = (new Query)
                ->select('s.id,s.name,s.id_type_product,s.title_url,t.title_url as `tp_title_url`')
                ->from('sub_type_product s')
                ->leftJoin('type_product t','s.id_type_product = t.id')
                ->where(['s.status' => '1'])
                ->orderBy('s.positions desc,s.id desc')
                ->all();
        return $result;
    }
    public static function getSubTypeProduct($id) {
        $result = (new Query)
                ->select('id,name')
                ->from(static::tableName())
                ->where(['id_type_product' => $id])
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    /**
    * cập nhật dữ liệu từ form 
    * [BACKEND ADD - UPDATE] 
    * [CONTROLLER ADD - UPDATE] [PHP]
    **/
    public static function updateSubTypeProduct($dataForm) {
        if(!empty($dataForm['id'])){ // nếu là update sub-type-product
            $model = SubTypeProduct::findOne($dataForm['id']);
        }
        else { // nếu là thêm mới product
            $model = new SubTypeProduct();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
            $model->positions = SubTypeProduct::find()->max('id') + 1;
        }

        
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->title = ! empty($dataForm['title']) ? $dataForm['title'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->legation = ! empty($dataForm['legation']) ? $dataForm['legation'] : "" ;
        $model->short_description = ! empty($dataForm['short_description']) ? $dataForm['short_description'] : "" ;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        $model->id_type_product = ! empty($dataForm['typeProduct']) ? $dataForm['typeProduct'] : 0;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        $model->save();
        return $model->id;  
    }
    public static function getSubTypeProductFollowTypeProduct($id) {
        $result = (new Query)
                ->select('p.title_url,p.name,p.description,p.image,p.id_type_product,tp.name as `tp_name`,tp.id as tp_id,p.short_description ,st.name as `st_name` , tp.title_url as `tp_title_url`,st.title_url as `st_title_url`')
                ->from('product p')
                ->leftJoin('sub_type_product st', 'p.id_sub_type_product = st.id')
                ->leftJoin('type_product tp','p.id_type_product = tp.id')
                ->where(['p.status' => '1','st.title_url' => $id])
                // ->where(['position' => '1'])
                ->all();
        return $result;

    }
    /* 
    INDEX (hiển thị) dịch vụ được quan tâm
    LIMIT 3 RANDOM
    */
    public static function getSubTypeProductIndex($id){
        $result = (new Query)
                ->select('s.id,s.name,s.image,s.title_url,t.title_url as `t_title_url`')
                ->from('sub_type_product s')
                ->leftJoin('type_product t','s.id_type_product = t.id')
                ->where(['s.status'=> 1])
                ->andWhere(['s.id_type_product' => $id])
                ->andWhere(['<>','s.title_url',''])
                ->orderBy(new Expression('rand()'))
                ->limit(3)
                ->all();
        return $result;
    }
    public static function getSubTypeProductContent($id){
        $result = (new Query)
                ->select('st.id,st.name,st.title_url,st.description,st.legation,t.title_url as `tp_title_url`,t.name as `tp_name`')
                ->from('sub_type_product st')
                ->leftJoin('type_product t','st.id_type_product = t.id')
                ->where(['st.title_url' => $id])
                ->one();
        return $result;
    }
    // public static function getSubTypeProductIndex(){
    //     $result = (new Query)
    //             ->select('id,name,image')
    //             ->from(static::tableName())
    //             ->where(['status'=> 1])
    //             ->andWhere(['id_type_product' => 20])
    //             // ->andWhere(['position' => 2,)
    //             ->limit(6)
    //             ->orderBy('id desc')
    //             ->all();
    //     return $result;
    // }
     public static function deleteSubTypeProduct($id){
        $model =SubTypeProduct::findOne($id);
        return $model->delete();
    }
    public static function getDocument($id){
        $result = (new Query)
                ->select('d.id,d.name,d.title_url,t.title_url as `tp_title_url`,t.name as `tp_name`,st.name as `st_name`,st.title_url as `st_titlle_url`')
                ->from('document d')
                ->leftJoin('sub_type_product st','d.id_sub_type_product = st.id')
                ->leftJoin('type_product t','st.id_type_product = t.id')
                ->where(['st.title_url' => $id])
                ->andWhere(['d.status'=> 1])
                ->all();
        return $result;
        
    }
    /* 
    Chi tiết tài liệu hướng dẫn
    [Sub-type-product-controller documentDetail]
    */
    public static function getDocumentDetail($id){
        $result = (new Query)
                ->select('d.id,d.name,d.title_url,d.description,t.title_url as `tp_title_url`,t.name as `tp_name`,st.name as `st_name`,st.title_url as `st_titlle_url`')
                ->from('document d')
                ->leftJoin('sub_type_product st','d.id_sub_type_product = st.id')
                ->leftJoin('type_product t','st.id_type_product = t.id')
                ->where(['d.title_url' => $id])
                // ->andWhere(['d.status'=> 1])
                ->one();
        return $result;
        
    }
    


}
