<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;

/**
 * Slider model
 *
 * @property integer $id
 * @property string $title
 * @property string $description    
 * @property string $image
 * @property string $status
 */
class Slider extends ActiveRecord 
{
  
    public static function tableName()
    {
        return '{{%slider}}';
    }
    /**
     * Lấy tất cả sliderr trong db
     * @return array [thông tin slider]
     */
    public static function getAllSlider(){
        $result = (new Query)
                ->select('id,name,description,image,status,positions')
                ->from(static::tableName())
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('slider', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /**
    * Hiển thị hay không hiển thị bài
    **/
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update(static::tableName(), ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * Lưu form slider
     * @param $dataForm [dữ liệu từ form slider gửi lên server]
     */
    public static function updateSlider($dataForm) {
        if(!empty($dataForm['id'])){
            // nếu là update slider
            $model = Slider::findOne($dataForm['id']);
        }
        else{
            // nếu là thêm mới slider
            $model = new Slider();
            $model->positions = Slider::find()->max('id') + 1;
        }
        
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->link = ! empty($dataForm['link']) ? $dataForm['link'] : "" ;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
      
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
    public static function deleteSlider($id){
        $model =Slider::findOne($id);
        return $model->delete();
    }
    
    /* 
    * Lấy tất cả slider đang hoạt động 
    * [FRONTEND] [SLIDER INDEX]
    */
    public static function getSlider(){
        $result = (new Query)
                ->select('image,link')
                ->from(static::tableName())
                ->where(['status' => 1])
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
   


}
