<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;

/**
 * Setting model
 *
 * @property integer $id
 * @property string $logo_image
 * @property string $addess
 * @property string $phone
 * @property string $email
 * @property string $link_youtube
 * @property string $link_facebook
 * @property integer $link_twitter
 */
class Setting extends ActiveRecord 
{
	// public $email;
 //    public $address;
 //    public $logo_image;
 //    public $phone;
 //    public $link_youtube;
 //    public $link_facebook;
 //    public $link_twitter;
  
    public static function tableName()
    {
        return '{{%setting}}';
    }
    public static function getAllSetting() {
    	$result = (new Query)
                ->select('*')
                ->from(static::tableName())
                ->one();
        return $result;
    }
    
    /**
	 * Save form setting
	 * @param $dataForm [dữ liệu từ form setting gửi lên server]
	 */
    public static function updateSetting($dataForm) {
    	$model = Setting::findOne($dataForm['id']);
		$model->email = ! empty($dataForm['email']) ? $dataForm['email'] : "" ;
		$model->address = ! empty($dataForm['address']) ? $dataForm['address'] : "" ;
        $model->phone = ! empty($dataForm['phone']) ? $dataForm['phone'] : "" ;
        $model->mobilephone = ! empty($dataForm['mobilephone']) ? $dataForm['mobilephone'] : "" ;
		$model->work_time = ! empty($dataForm['workTime']) ? $dataForm['workTime'] : "" ;
        $model->link_facebook = ! empty($dataForm['linkFacebook']) ? $dataForm['linkFacebook'] : "" ; 

        $model->link_skype = ! empty($dataForm['linkSkype']) ? $dataForm['linkSkype'] : "" ;
        $model->link_viber = ! empty($dataForm['linkViber']) ? $dataForm['linkViber'] : "" ;
        $model->link_printerest = ! empty($dataForm['linkPrinterest']) ? $dataForm['linkPrinterest'] : "" ;
		$model->link_instagram = ! empty($dataForm['linkInstagram']) ? $dataForm['linkInstagram'] : "" ;
        $model->link_twitter = ! empty($dataForm['linkTwitter']) ? $dataForm['linkTwitter'] : "" ;
        $model->link_map = ! empty($dataForm['linkMap']) ? $dataForm['linkMap'] : "" ;
        $model->content_footer = ! empty($dataForm['contentFooter']) ? $dataForm['contentFooter'] : "" ;
		$model->content_contact = ! empty($dataForm['contentContact']) ? $dataForm['contentContact'] : "" ;
        if(! empty($dataForm['logo'])){
            $model->logo_image = $dataForm['logo'];
        }
        if(! empty($dataForm['bannerPost'])){
            $model->banner_post = $dataForm['bannerPost'];
        }
        if(! empty($dataForm['bannerVisa'])){
            $model->banner_visa = $dataForm['bannerVisa'];
        }
		return $model->save();  
    }


}
