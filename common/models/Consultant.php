<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use yii\behaviors\SluggableBehavior;
class Consultant extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%consultant}}';
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('consultant', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('consultant', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
   
    /**
     * Save form post
     * @param $dataForm [dữ liệu từ form post gửi lên server]
     */
    public static function updateConsultant($dataForm) {
        if(!empty($dataForm['id'])){
            // nếu là update post
            $model = Consultant::findOne($dataForm['id']);
        }
        else{
            // nếu là thêm mới post
            $model = new Consultant();
            $model->positions = Consultant::find()->max('id') + 1;
        }
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->job = ! empty($dataForm['job']) ? $dataForm['job'] : "" ;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
    /**
    * Danh sách tất cả post
    * [BACKEND INDEX] 
    **/
    public static function getAllConsultant(){
        $result = (new Query)
                ->select('id,name,description,image,status,positions,job')
                ->from(static::tableName())
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    public static function deleteConsultant($id){
        $model = Consultant::findOne($id);
        return $model->delete();
    }
    public static function getConsultant(){
        $result = (new Query)
                ->select('id,name,description,image,status,positions,job')
                ->from('consultant p')
                ->where(['p.status' => 1])
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    /**
    * Bài viết trang index
    * [FRONTEND INDEX] 
    **/
    // public static function getPost(){
    //     $result = (new Query)
    //             ->select('p.title_url,p.id,p.name,p.image,p.description,p.id_type_post,p.email,FROM_UNIXTIME(p.time_create) as `time_create`,p.short_description,tp.title_url as `tp_title_url`')
    //             ->from('post p')
    //             ->leftJoin('type_post tp', 'p.id_type_post = tp.id')
    //             ->where(['p.status' => 1])
    //             ->andWhere(['!=', 'p.id', 2]) // remove bài về chúng tôi
    //             ->orderBy('p.positions desc')
    //             ->all();
    //     return $result;
    // }
    /**
    * Chi tiết bài viết
    * [FRONTEND DETAIL-POST] 
    **/
    // public static function getPostId($id) {
    //     $result = (new Query)
    //         ->select('tp.title_url,p.description,p.name,tp.id as tp_id,tp.name as tp_name')
    //         ->from('post p')
    //         ->leftJoin('type_post tp', 'p.id_type_post = tp.id')
    //         ->where(['p.status' => 1,'p.title_url' => $id])
    //         ->one();
    //     return $result;
    // }
    /**
    * Danh sách bài post liên quan
    * [FRONTEND DETAIL-POST] 
    **/
    // public static function relatePost ($id,$idTypePost) {
    //     $result = (new Query)
    //             ->select('p.name,p.description,p.id_type_post,p.id,p.title_url,tp.title_url as `tp_title_url`')
    //             ->from('post p')
    //             ->leftJoin('type_post tp', 'p.id_type_post = tp.id')
    //             ->where(['p.status' => '1','p.id_type_post' => $idTypePost])
    //             ->andWhere(['not in','p.title_url',[$id]])
    //             ->limit('4')
    //             ->orderBy('p.positions desc')
    //             ->all();
    //     return $result;
    // }
}