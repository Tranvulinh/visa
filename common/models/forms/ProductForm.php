<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\Product;
/**
 * ProductForm
 */
class ProductForm extends Model
{
    /**
     * {@inheritdoc}
     */
    // public $image;
    // public $email;

    // public $name;
    public $id;
    public $image;
    public $contentVI;
    public $contentEN;
    public $contentshortVI;
    public $contentshortEN;
    public $nameVI;
    public $nameEN;
    public $typeProduct;
    public $subTypeProduct;
    public $countryProduct;
    public $titleURL;
    // public $position;
    public $status;

    public function rules()
    {
        return [
           
            ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên bài viết visa'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
     public function upload()
    {
            $link = 'img/product/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/product/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = Product::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Tiêu đề này đã được sử dụng hãy thay đổi tên khác');
        }
    }


}
