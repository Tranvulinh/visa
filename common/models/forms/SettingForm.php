<?php
namespace common\models\forms;
use Yii;
use yii\base\Model;
/**
 * SettingForm
 */
class SettingForm extends Model
{
    public $email;
    public $address;
    public $phone;
    public $mobilephone;
    public $workTime;
    public $logo;
    public $linkFacebook;
    public $linkSkype;
    public $linkViber;
    public $linkPrinterest;
    public $linkInstagram;
    public $linkTwitter; 
    public $linkLink; 
    public $contentFooter;
    public $contentFooterVI;
    public $contentFooterEN;
    public $bannerPost;
    public $bannerVisa;
    public function rules()
    {
        return [
            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
            [['bannerPost'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
            [['bannerVisa'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
        ];
    }
    /* Upload hình ảnh logo */ 
    public function upload()
    {
        if ($this->validate()) {
            $link = 'img/logo/' . $this->logo->baseName . '.' . $this->logo->extension;       
            $this->logo->saveAs(Yii::getAlias('@frontend').'/web/img/logo/' . $this->logo->baseName . '.' . $this->logo->extension);
            return $link;
        } else {
            return false;
        }
    }
    /* Upload hình ảnh banner trên mỗi bài post */
    public function uploadBannerPost()
    {
        if ($this->validate()) {
            $link = 'img/banner/' . $this->bannerPost->baseName . '.' . $this->bannerPost->extension;       
            $this->bannerPost->saveAs(Yii::getAlias('@frontend').'/web/img/banner/' . $this->bannerPost->baseName . '.' . $this->bannerPost->extension);
            return $link;
        } else {
            return false;
        }
    }
    /* Upload hình ảnh banner trên mỗi bài visa */

    public function uploadBannerVisa()
    {
        if ($this->validate()) {
            $link = 'img/banner/' . $this->bannerVisa->baseName . '.' . $this->bannerVisa->extension;       
            $this->bannerVisa->saveAs(Yii::getAlias('@frontend').'/web/img/banner/' . $this->bannerVisa->baseName . '.' . $this->bannerVisa->extension);
            return $link;
        } else {
            return false;
        }
    }
}