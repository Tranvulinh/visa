<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;

/**
 * SliderForm
 */
class UploadForm extends Model
{
    /**
     * {@inheritdoc}
     */
    public $image;
    // public $email;

    public function rules()
    {
        return [
          
           [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
       ];
    }
     public function upload()
    {
        // return $this->image;
        $link = 'img/upload/' . $this->image->baseName . '.' . $this->image->extension;       
        $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/upload/' . $this->image->baseName . '.' . $this->image->extension);
        return $link;
    }



}
