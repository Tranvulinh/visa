<?php
namespace common\models\forms;
use Yii;
use yii\base\Model;
/**
 * SliderForm
 */
class SliderForm extends Model
{
    /**
     * {@inheritdoc}
     */
    public $image;
    public $link;
    public $status;
    public $contentVI;
    public $contentEN;
    public $nameVI;
    public $nameEN;
    public function rules()
    {
        return [
           [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
       ];
    }
    /* Upload Slider */
    public function upload()
    {
        $link = 'img/slider/' . $this->image->baseName . '.' . $this->image->extension;       
        $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/slider/' . $this->image->baseName . '.' . $this->image->extension);
        return $link;
    }
}