<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\TypeProduct;


/**
 * Type Product Form
 */
class TypeProductForm extends Model
{
    /**
     * {@inheritdoc}
     */
    public $name;
    public $id;
    public $image;
    public $contentVI;
    public $contentEN;
    public $nameVI;
    public $nameEN;
    public $position;
    public $status;
    
   
    public function rules()
    {
        return [
           
            ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên loại Visa'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
    
    public function upload()
    {
            $link = 'img/type_product/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/type_product/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = TypeProduct::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Tiêu đề này đã được sử dụng hãy đặt tên khác');
        }
    }

       


}
