<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\SubTypeProduct;
/**
 * Sub Type Product Form
 */
class SubTypeProductForm extends Model
{
    /**
     * {@inheritdoc}
     */
    // public $name;
    public $image;
    public $id;
    public $contentVI;
    public $contentEN;
    public $legationVI;
    public $legationEN;
    public $contentshortVI;
    public $contentshortEN;
    public $nameVI;
    public $nameEN;
    public $typeProduct;
    public $dynamicInputVI = [];
    public $dynamicInputEN = [];
    public $dynamicContentVI = [];
    public $dynamicContentEN = [];
    public $status;
    
   
    public function rules()
    {
        return [
           
            ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên Visa'],
            
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
    
    public function upload()
    {
            $link = 'img/sub_type_product/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/sub_type_product/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = SubTypeProduct::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Title đã tồn tại');
        }
    }

       


}
