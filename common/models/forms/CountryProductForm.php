<?php
namespace common\models\forms;
use Yii;
use yii\base\Model;
use common\models\CountryProduct;
class CountryProductForm extends Model
{
    public $image;
    public $nameVI;
    public $nameEN;
    public $titleURL;
    public $status;
    public $subTypeProduct;
    public $dynamicInputVI = [];
    public $dynamicInputEN = [];
    public $dynamicContentVI = [];
    public $dynamicContentEN = [];
    public $id;
    public function rules()
    {
        return [
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên nước'],
            ['nameVI', 'checkUniqueName'],
            // [['dynamicInputVI'], 'each', 'rule' => ['required','message' => 'Không được để rỗng']],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
        ];
    }
     public function upload()
    {
            $link = 'img/country_product/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/country_product/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = CountryProduct::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Nước này đã tồn tại hãy thay đổi tên nước khác');
        }
    }
}