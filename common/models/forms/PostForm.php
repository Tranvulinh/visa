<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\Post;
/**
 * PostForm
 */
class PostForm extends Model
{
    /**
     * {@inheritdoc}
     */
    // public $image;
    // public $email;

    // public $name;
    public $id;
    public $image;
    public $contentVI;
    public $contentEN;
    public $contentshortVI;
    public $contentshortEN;
    public $nameVI;
    public $nameEN;
    public $typePost;
    public $position;
    public $status;

    public function rules()
    {
        return [
           
            ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên bài viết'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
     public function upload()
    {
            $link = 'img/post/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/post/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = Post::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Tile đã tồn tại');
        }
    }


}
