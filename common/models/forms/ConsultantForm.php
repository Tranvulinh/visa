<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\Post;
/**
 * PostForm
 */
class ConsultantForm extends Model
{
    /**
     * {@inheritdoc}
     */
    // public $image;
    // public $email;

    // public $name;
    public $id;
    public $image;
    public $contentVI;
    public $contentEN;
    public $jobVI;
    public $jobEN;
    public $name;
    // public $name;
    // public $typePost;
    public $positions;
    public $status;

    public function rules()
    {
        return [
           
            // ['nameVI', 'checkUniqueName'],
            ['name', 'required', 'message' => 'Vui lòng nhập tên'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
     public function upload()
    {
            $link = 'img/consultant/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/consultant/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
}
