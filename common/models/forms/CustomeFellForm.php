<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\Post;
/**
 * PostForm
 */
class CustomeFellForm extends Model
{
    /**
     * {@inheritdoc}
     */
    // public $image;
    // public $email;

    // public $name;
    public $id;
    public $image;
    public $contentVI;
    public $contentEN;
    public $companyVI;
    public $companyEN;
    public $nameVI;
    public $nameEN;
    // public $typePost;
    public $positions;
    public $status;

    public function rules()
    {
        return [
           
            // ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],

           
        ];
    }
     public function upload()
    {
            $link = 'img/custome_fell/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/custome_fell/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
}
