<?php
namespace common\models\forms;
use Yii;
use yii\base\Model;
use common\models\TypePost;
/**
 * Type Product Form
 */
class TypePostForm extends Model
{
    /**
     * {@inheritdoc}
     */
    public $name;
    public $id;
    public $image;
    public $contentshortVI;
    public $contentshortEN;
    public $contentVI;
    public $contentEN;
    public $nameVI;
    public $nameEN;
    // public $position;
    public $status;
    public function rules()
    {
        return [
            ['nameVI', 'checkUniqueName'],
            ['nameVI', 'required', 'message' => 'Vui lòng nhập tên loại Visa'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg','wrongExtension' => 'Vui lòng chọn file hình ảnh có định dạng png/jpg/jpeg'],
        ];
    }
    public function upload()
    {
            $link = 'img/type_post/' . $this->image->baseName . '.' . $this->image->extension;       
            $this->image->saveAs(Yii::getAlias('@frontend').'/web/img/type_post/' . $this->image->baseName . '.' . $this->image->extension);
            return $link;
    }
    public function checkUniqueName($attribute, $params, $validator)
    {
        $isUnique = TypePost::checkUniqueName($this->$attribute,$this->id);
        if(!empty($isUnique)) {
            $this->addError($attribute, 'Title đã tồn tại');
        }
    }
}