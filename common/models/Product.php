<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use yii\behaviors\SluggableBehavior;
/**
 * Product model
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property string $time_create
 * @property string $email
 * @property string $plus_info
 * @property integer $status
 * @property integer $id_sub_type_product
 */
class Product extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%product}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('product', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /**
    * Kiểm tra trùng title 
    * [BACKEND ADD-UPDATE] 
    * [CONTROLLER PRODUCT] [ADD - UPDATE]
    **/
    public static function checkUniqueName($title,$id = null) {
         $result = (new Query)
                ->select('p.title')
                ->from('product p')
                ->where(['p.title' => $title])
                ->andWhere(['<>','p.id' , $id])
                ->one();
    return $result;
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('product', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * Save form product
     * @param $dataForm [dữ liệu từ form product gửi lên server]
     */
    public static function updateProduct($dataForm) {
        if(!empty($dataForm['id'])){
            // nếu là update product
            $model = Product::findOne($dataForm['id']);
        }
        else{
            // nếu là thêm mới product
            $model = new Product();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
            $model->positions = Product::find()->max('id') + 1;
        }
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->title = ! empty($dataForm['title']) ? $dataForm['title'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->short_description = ! empty($dataForm['short_description']) ? $dataForm['short_description'] : "" ;
        $model->id_type_product = ! empty($dataForm['typeProduct']) ? $dataForm['typeProduct'] : 0;
        $model->id_sub_type_product = ! empty($dataForm['subTypeProduct']) ? $dataForm['subTypeProduct'] : 0;
        $model->id_country_product = ! empty($dataForm['countryProduct']) ? $dataForm['countryProduct'] : 0;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
    /* 
    * Danh sách tất cả bài viết visa
    * [BACKEND INDEX] 
    */
    public static function getAllProduct(){
        $result = (new Query)
                ->select('id,name,description,image,status,time_create,title_url,positions')
                ->from(static::tableName())
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    public static function deleteProduct($id){
        $model = Product::findOne($id);
        return $model->delete();
    }
    // public static function productMenu() {
    //     $result = (new Query)
    //             ->select('id,name,id_type_product,title_url')
    //             ->from(static::tableName())
    //             ->where(['status' => '1'])
    //             ->orderBy('id desc')
    //             // ->where()
    //             ->all();
    //     return $result;
    // }
    public static function getProductId($id) {
        $result = (new Query)
                ->select('p.title_url,p.name,p.description,p.id_type_product,tp.id as tp_id,tp.name as tp_name,tp.title_url as `tp_title_url`,st.title_url as `st_title_url`,st.name as `st_name`')
                ->from('product p')
                ->leftJoin('sub_type_product st','p.id_sub_type_product = st.id')
                ->leftJoin('type_product tp', 'p.id_type_product = tp.id')
                ->where(['p.status' => '1','p.title_url' => $id])
                // ->where(['position' => '1'])
                ->one();
        return $result;
    }
    public static function relateProduct ($id,$idTypeProduct) {
        $result = (new Query)
                ->select('p.name,p.description,p.id_type_product,p.id,title_url')
                ->from('product p')
                // ->leftJoin('type_product tp', 'p.id_type_product = tp.id')
                ->where(['p.status' => '1','p.id_type_product' => $idTypeProduct])
                ->andWhere(['not in','p.id',[$id]])
                // ->where(['position' => '1'])
                ->limit('4')
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
    public static function search ($search) {
        // $search = json_encode($search);
        // print_r($search);
        $search = str_replace('"','',json_encode($search));
        // die();
        $result = (new Query)
                ->select('p.name,p.description,p.id_type_product,p.id,p.image,p.short_description,p.title_url')
                ->from('product p')
                // ->leftJoin('type_product tp', 'p.id_type_product = tp.id')
                ->where(['p.status' => '1'])
                ->andWhere(['like', 'name', $search])
                ->orderBy('positions desc,id desc')
                ->all();
        return $result;
    }
}