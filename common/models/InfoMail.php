<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;

class InfoMail extends ActiveRecord 
{


    /**
	 * Save form InfoMail
	 * @param $dataForm [dữ liệu từ form setting gửi lên server]
	 */
    public static function updateInfoMail($dataForm) {
  
        $model = new InfoMail();
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->email = ! empty($dataForm['email']) ? $dataForm['email'] : "" ;
        $model->phone = ! empty($dataForm['phone']) ? $dataForm['phone'] : "" ;
        $model->address = ! empty($dataForm['address']) ? $dataForm['address'] : "" ;
        $model->message = ! empty($dataForm['message']) ? $dataForm['message'] : "";
        
        return $model->save();  
    }
    public static function getAllInfoMail() {
        // public function getAllSlider(){
            $result = (new Query)
                    ->select('id,name,email,phone,address,message')
                    ->from(static::tableName())
                    ->orderBy('id desc')
                    ->all();
            return $result;
        // }
    } 

}
