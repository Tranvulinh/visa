<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use yii\behaviors\SluggableBehavior;
/**
 */
class Document extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%document}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /* 
    * Lấy tất cả các nước 
    * [show index backend country- product]
    */
    public static function getAllDocumentSub($idSub){
        $result = (new Query)
                ->select('id,name,description')
                ->from(static::tableName())
                ->where(['id_sub_type_product' => $idSub])
                ->all();
        return $result;
    }
    public static function removeDocument($id){
        (new Query)
        ->createCommand()
        ->delete('document', ['id' => $id])
        ->execute();
    }
    public static function updateDocument($dataForm) {
        if(!empty($dataForm['idDo'])){ // nếu là update product
            $model = Document::findOne($dataForm['idDo']);
        }
        else { // nếu là thêm mới product
            $model = new Document();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
        }
        $model->name = ! empty($dataForm['nameDo']) ? $dataForm['nameDo'] : "" ;
        $model->title = ! empty($dataForm['titleDo']) ? $dataForm['titleDo'] : "" ;
        $model->description = ! empty($dataForm['descriptionDo']) ? $dataForm['descriptionDo'] : "" ;
        $model->id_sub_type_product = ! empty($dataForm['sub']) ? $dataForm['sub'] : 0;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        // if(! empty($dataForm['image'])){
        //     $model->image = $dataForm['image'];
        // }
        return $model->save();  
    }
}