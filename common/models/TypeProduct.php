<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use common\models\TypeProduct;
use yii\behaviors\SluggableBehavior;
/**
 * Slider model
 *
 * @property integer $id
 * @property string $title
 * @property string $description    
 * @property string $image
 * @property string $status
 */
class TypeProduct extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%type_product}}';
    }
    /* Tạo slug từ 1 cột */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('type_product', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('type_product', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
    * Kiểm tra trùng title 
    * [BACKEND ADD - UPDATE] 
    * [CONTROLLER ADD - UPDATE] [PHP]
    **/
    public static function checkUniqueName($title,$id = null) {
         $result = (new Query)
                ->select('p.title')
                ->from('type_product p')
                ->where(['p.title' => $title])
                ->andWhere(['<>','p.id' , $id])
                ->one();
        return $result;
    }
    /* 
    * Danh sách tất cả loại visa
    * [BACKEND INDEX]
    */
    public static function getAllTypeProduct() {
        $result = (new Query)
                ->select('id,name,description,image,time_create,status,positions')
                ->from(static::tableName())
                ->orderBy('positions desc')
                ->all();
        return $result;
    }
    /**
    * cập nhật dữ liệu từ form 
    * [BACKEND ADD - UPDATE] 
    * [CONTROLLER ADD - UPDATE] [PHP]
    **/
    public static function updateTypeProduct($dataForm) {
        if(!empty($dataForm['id'])) { // Nếu đã có tồn tại id
            $model = TypeProduct::findOne($dataForm['id']);
        }
        else { 
            $model = new TypeProduct();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
            $model->positions = TypeProduct::find()->max('id') + 1;
        }
        $model->title = ! empty($dataForm['title']) ? $dataForm['title'] : "" ;
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
     public static function deleteTypeProduct($id){
        $model = TypeProduct::findOne($id);
        return $model->delete();
    }
    /**
    * Lấy loại visa trên menu
    * [FRONTEND] [CONTROLLER CHUNG]
    **/
    public static function getTypeProductMenu(){
        $result = (new Query)
                ->select('tp.id,tp.name,tp.title_url')
                ->from('type_product tp')
                ->where(['status'=> 1])
                ->limit(3)
                ->orderBy('tp.positions desc')
                ->all();
        return $result;
    }
    // public static function getTypeProductIndex(){
    //     $result = (new Query)
    //             ->select('id,name,image')
    //             ->from(static::tableName())
    //             ->where(['status'=> 1])
    //             // ->andWhere(['id' => 20])
    //             // ->andWhere(['position' => 2,)
    //             ->limit(4)
    //             ->orderBy('id desc')
    //             ->all();
    //     return $result;
    // }

    /**
    * Tất cả bài VISA thuộc LOẠI VISA
    * [FRONTEND TYPE-PRODUCT] 
    **/
    public static function getProductFollowTypeProduct($id) {
        $result = (new Query)
                ->select('p.id,p.name,p.description,p.short_description,p.image,p.id_type_product,tp.id as tp_id,tp.name as tp_name,p.title_url,tp.title_url as `tp_title_url`, tp.id as `idTP`')
                ->from('sub_type_product p')
                ->leftJoin('type_product tp', 'p.id_type_product = tp.id')
                ->where(['p.status' => '1','tp.title_url' => $id])
                // ->orderBy('tp.positions desc')
                ->orderBy('p.positions desc,p.id desc')
                ->all();
        return $result;
    }
}