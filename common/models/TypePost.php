<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Common;
use common\models\TypePost;
use yii\behaviors\SluggableBehavior;
class TypePost extends ActiveRecord 
{
    public static function tableName()
    {
        return '{{%type_post}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title', // cột để phân tách thành slug
                'slugAttribute' => 'title_url', // cột lưu slug
            ],
        ];
    }
    /**
    * Thay đổi vị trí của bài viết
    * [BACKEND INDEX] [DRAG-DROP] [JQUERY]
    **/
    public static function changePosition($position) {
        $connection = Yii::$app->db;
        foreach($position as $p) {
            $index = $p[0];
            $newPosition = $p[1];
            $connection->createCommand()
            ->update('type_post', ['positions' => $newPosition], ['id' => $index])
            ->execute();
        }
    }
    /* 
    * Thay đổi checkbox khi nhấn button
    * [BACKEND INDEX] [TOGGLE CHECKBOX] [JQUERY]
    */
    public static function changeStatus($id,$status) {
        $connection = Yii::$app->db;
        $change = $connection->createCommand()
        ->update('type_post', ['status' => $status], ['id' => $id])
        ->execute();
        if($change) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
    * Kiểm tra trùng title 
    * [BACKEND ADD-UPDATE] 
    * [CONTROLLER PRODUCT] [ADD - UPDATE]
    **/
    public static function checkUniqueName($title,$id = null) {
        $result = (new Query)
            ->select('p.title')
            ->from('type_post p')
            ->where(['p.title' => $title])
            ->andWhere(['<>','p.id' , $id])
            ->one();
        return $result;
    }
    /**
    * Danh sách tất cả loại post
    * [BACKEND INDEX] 
    **/
    public static function getAllTypePost() {
        $result = (new Query)
            ->select('id,name,description,image,status,short_description,positions')
            ->from(static::tableName())
            ->orderBy('positions desc,id desc')
            ->all();
        return $result;
    }
     public static function updateTypePost($dataForm) {
        if(!empty($dataForm['id'])){
            // nếu là update product
            $model = TypePost::findOne($dataForm['id']);
        }
        else{
            // nếu là thêm mới product
            $model = new TypePost();
            $model->time_create = time();
            $model->email = Yii::$app->user->identity->username;
            $model->positions = TypePost::find()->max('id') + 1;
        }
        $model->title = ! empty($dataForm['title']) ? $dataForm['title'] : "" ;
        $model->name = ! empty($dataForm['name']) ? $dataForm['name'] : "" ;
        $model->description = ! empty($dataForm['description']) ? $dataForm['description'] : "" ;
        $model->short_description = ! empty($dataForm['short_description']) ? $dataForm['short_description'] : "" ;
        $model->time_create = time();
        $model->status = !empty($dataForm['status']) ? 1 : 0 ;
        if(! empty($dataForm['image'])){
            $model->image = $dataForm['image'];
        }
        return $model->save();  
    }
    public static function deleteTypePost($id){
        $model =TypePost::findOne($id);
        return $model->delete();
    }
    /**
    * Danh sách loại post
    * [FRONTEND INDEX] 
    **/
    public static function getAllTypePostStatus() {
        $result = (new Query)
            ->select('id,name,description,image,status,short_description,title_url')
            ->from(static::tableName())
            ->where(['status' => 1])
            ->orderBy('positions desc,id desc')
            ->all();
        return $result;
    }
    /**
    * Tất cả bài viết thuộc loại bài viết
    * [FRONTEND TYPE-POST] 
    **/
    public static function getPostFollowTypePost($id) {
        $result = (new Query)
            ->select('p.title_url,p.id,p.name,p.description,p.short_description,p.image,p.id_type_post,tp.id as tp_id,tp.name as tp_name,tp.title_url as `tp_title_url`')
            ->from('post p')
            ->leftJoin('type_post tp', 'p.id_type_post = tp.id')
            ->where(['p.status' => '1','tp.title_url' => $id])
            ->orderBy('p.positions desc,p.id desc')
            ->all();
        return $result;
    }
}