<?php
// set up trong common/main
return [
    'Search .....' => 'Tìm Kiếm .....',
    'Telephone : ' => 'Điện thoại : ',
    'Hotline : ' => 'Hotline :',
    'Address : ' => 'Địa chỉ : ',
    'Address' => 'Địa chỉ',
    'Contact' => 'Liên hệ',
    'Message' => 'Lời nhắn',
    'CONTACT WITH US' => 'THÔNG TIN LIÊN HỆ',
    'Name' => 'Tên', 
    'Email' => 'Địa chỉ email', 
    'Phone' => 'Điện thoại di động', 
    'Send' => 'Đăng kí', 
    'Home' => 'Trang chủ', 
    'Details' => 'Xem chi tiết', 
    'Relate Post' => 'BÀI VIẾT LIÊN QUAN',
    'HANDLING<br>FAST PROFILE'  => "xử lí <br />hồ sơ nhanh",
    'Free<br>consultation'  => "miễn phí<br />tư vấn",
    'PERCENTAGE RATE <br> 99%'  => "tỉ lệ đạt<br />99%",
    'Cost <br> savings'  => "Tiết kiệm <br> chi phí",
    'RESPONSIBILITY TO <br>CUSTOMER'  => "Trách nhiệm với khách hàng",
    'PROCEDURE<br>PROFESSION'  => "Quy trình chuyên nghiệp",
    'WHY CHOOSE US' => 'TẠI SAO CHỌN CHÚNG TÔI',
    'SERVICES ARE INTERESTED' => 'dịch vụ được quan tâm',
    'NEWS AND EXPERIENCE' => 'TIN TỨC VÀ KINH NGHIỆM',
    'Read more' => 'Đọc Tiếp',
    'ONLINE SUPPORT' => 'HỖ TRỢ TRỰC TUYẾN',
    'Nothing to show' => 'Không có dữ liệu',
    'Customer Service' => 'Chăm sóc khách hàng',
    'Customer Review' => 'Cảm nhận khách hàng',
    'Post' => 'Tin Tức',
    'Thời gian làm việc :' => 'Time work : '


    

];