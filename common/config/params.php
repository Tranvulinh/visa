<?php
return [
    'adminEmail' => 'fptplayboxfshare@gmail.com',
    'supportEmail' => 'fptplayboxfshare@gmail.com',
    'senderEmail' => 'fptplayboxfshare@gmail.com',
    'senderName' => 'fptplayboxfshare@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
