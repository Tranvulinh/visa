﻿Composer

step 1 : composer create-project --prefer-dist yiisoft/yii2-app-advanced yii-application 
step 2 : composer install (chạy composer json)
step 3 : php init (tạo main-local)
step 4 : config vhost 

<VirtualHost *:80>
        ServerName frontend.test
        DocumentRoot "C:/xampp/htdocs/try-your-best/frontend/web/"
        
        <Directory "C:/xampp/htdocs/try-your-best/frontend/web/">
            # use mod_rewrite for pretty URL support
            RewriteEngine on
            # If a directory or a file exists, use the request directly
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteCond %{REQUEST_FILENAME} !-d
            # Otherwise forward the request to index.php
            RewriteRule . index.php

            # use index.php as index file
            DirectoryIndex index.php

            # ...other settings...
            # Apache 2.4
            Require all granted
            
            ## Apache 2.2
            # Order allow,deny
            # Allow from all
        </Directory>
    </VirtualHost>
    
    <VirtualHost *:80>
        ServerName backend.test
        DocumentRoot "C:/xampp/htdocs/try-your-best/backend/web/"
        
        <Directory "C:/xampp/htdocs/try-your-best/backend/web/">
            # use mod_rewrite for pretty URL support
            RewriteEngine on
            # If a directory or a file exists, use the request directly
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteCond %{REQUEST_FILENAME} !-d
            # Otherwise forward the request to index.php
            RewriteRule . index.php

            # use index.php as index file
            DirectoryIndex index.php

            # ...other settings...
            # Apache 2.4
            Require all granted
            
            ## Apache 2.2
            # Order allow,deny
            # Allow from all
        </Directory>
    </VirtualHost>