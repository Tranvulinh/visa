<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
// use yii\web\UploadedFile;
// use common\models\forms\PostForm;
use common\models\Product;
use yii\data\Pagination;
use yii\web\HttpException;
use common\models\Slider;
// use common\models\TypePost;
// use yii\helpers\ArrayHelper;
// use yii\filters\AccessControl;
// use yii\filters\VerbFilter;

/**
 * Post controller
 */
class ProductController extends Controller
{
    
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionDetailProduct() {
        $id = Yii::$app->request->get('id');
        $data = Product::getProductId($id);
        $slider = Slider::getSlider();
        // print_r($data);
        // die();
        if(empty($data)){
            throw new HttpException(404, Yii::t('app', 'Page not found'));
        }
        $dataRelate = Product::relateProduct($id,$data['tp_id']);
        return $this->render('product',
            [
                'data' => $data,
                'slider' => $slider,
                'dataRelate' => $dataRelate,
            ]
        );
    }
    public function actionSearch() {
        $search = Yii::$app->request->get('search');
        $data = Product::search($search);
        $count = count($data);


        $pagination = new Pagination(['totalCount' => $count]);
       
        return $this->render('search',
            [
                'data' => $data,
                'pagination' => $pagination,
            ]
        );
    }
    
    
  
        
       
        
}
   
