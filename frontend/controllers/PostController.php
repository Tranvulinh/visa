<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Post;
use common\models\Slider;
use yii\web\HttpException;
/**
 * Post controller
 */
class PostController extends Controller
{
    
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionDetailPost() {
        // die();
        $id = Yii::$app->request->get('id');
        $data = Post::getPostId($id);
        if(empty($data)){
            throw new HttpException(404, Yii::t('app', 'Page not found'));
        }
        $slider = Slider::getSlider();
        $dataRelate = Post::relatePost($id,$data['tp_id']);
        return $this->render('post',
            [
                'data' => $data,
                'dataRelate' => $dataRelate,
                'slider' => $slider,
            ]
        );
    }
    public function actionContact(){
        return $this->render('contact');
    }
    
    
  
        
       
        
}
   
