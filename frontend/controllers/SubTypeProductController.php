<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
// use yii\web\UploadedFile;
// use common\models\forms\PostForm;
use common\models\SubTypeProduct;
use common\models\CountryProduct;
use yii\data\Pagination;
// use common\models\TypePost;
// use yii\helpers\ArrayHelper;
// use yii\filters\AccessControl;
// use yii\filters\VerbFilter;

/**
 * Post controller
 */
class SubTypeProductController extends Controller
{
    
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionSubTypeProduct() {
        // echo "1";
        // die();
        // $query = Article::find()->where(['status' => 1]);

        // get the total number of articles (but do not fetch the article data yet)$id = $id = Yii::$app->request->get('id');
        $id = Yii::$app->request->get('id');
        $data = SubTypeProduct::getSubTypeProductContent($id);
        $country = CountryProduct::getCountryProductLink($id);
        // print_r($data);
        // die();
        // $data = SubTypeProduct::getSubTypeProductFollowTypeProduct($id);
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
        // $count = count($data);

        // create a pagination object with the total count
        // $pagination = new Pagination(['totalCount' => $count]);
        // print_r($pagination);
        // die();

        // // limit the query using the pagination and retrieve the articles
        // // $articles = $query->offset($pagination->4)
        // //     ->limit($pagination->limit)
        // //     ->all();
        // //     print_r($articles);

       
        // // $data = TypeProduct::getPostId($id);
        return $this->render('list-flag',
            [
                'data' => $data,
                'country' => $country,
                // 'pagination' => $pagination,
            ]
        );
    }
    public function actionType() {
        // echo "dfs";
        // die();
        // $query = Article::find()->where(['status' => 1]);

        // get the total number of articles (but do not fetch the article data yet)$id = $id = Yii::$app->request->get('id');
        $id = Yii::$app->request->get('id');
        // print_R($id);
        // die;
        // $data = SubTypeProduct::getSubTypeProductContent($id);
        // $country = CountryProduct::getCountryProductLink($id);
        // print_r($data);
        // die();
        $data = SubTypeProduct::getSubTypeProductFollowTypeProduct($id);
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
        // $count = count($data);

        // create a pagination object with the total count
        // $pagination = new Pagination(['totalCount' => $count]);
        // print_r($pagination);
        // die();

        // // limit the query using the pagination and retrieve the articles
        // // $articles = $query->offset($pagination->4)
        // //     ->limit($pagination->limit)
        // //     ->all();
        // //     print_r($articles);

       
        // // $data = TypeProduct::getPostId($id);
        return $this->render('sub_type_product',
            [
                'data' => $data,
                // 'country' => $country,
                // 'pagination' => $pagination,
            ]
        );
    }
    public function actionContent() {
        $id = Yii::$app->request->get('id');
        $data = SubTypeProduct::getSubTypeProductContent($id);
        return $this->render('content',
            [
                'data' => $data,
                // 'pagination' => $pagination,
            ]
        );
    }
    public function actionLegation() {
        $id = Yii::$app->request->get('id');
        $data = SubTypeProduct::getSubTypeProductContent($id);
        return $this->render('legation',
            [
                'data' => $data,
                // 'pagination' => $pagination,
            ]
        );
    }
    public function actionDocument() {
        $id = Yii::$app->request->get('id');
        $data = SubTypeProduct::getDocument($id);
        // print_r($data);
        // die;
        return $this->render('list-document',
            [
                'data' => $data,
                // 'pagination' => $pagination,
            ]
        );
    }

    public function actionDocumentDetail() {
        $id = Yii::$app->request->get('id');
        $data = SubTypeProduct::getDocumentDetail($id);
        // print_r($data);
        // die;
        return $this->render('detail',
            [
                'data' => $data,
                // 'pagination' => $pagination,
            ]
        );
    }
    
    
  
        
       
        
}
   
