<?php
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
// use yii\web\UploadedFile;
// use common\models\forms\PostForm;
use common\models\TypePost;
use common\models\Post;
use yii\data\Pagination;
// use common\models\TypePost;
// use yii\helpers\ArrayHelper;
// use yii\filters\AccessControl;
// use yii\filters\VerbFilter;
/**
 * Post controller
 */
class TypePostController extends Controller
{
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionTypePost() {
        // $query = Article::find()->where(['status' => 1]);
        // get the total number of articles (but do not fetch the article data yet)$id = $id = Yii::$app->request->get('id');
        $id = Yii::$app->request->get('id');
        $data = TypePost::getPostFollowTypePost($id);
        $count = count($data);
        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);
        // print_r($pagination);
        // die();
        // // limit the query using the pagination and retrieve the articles
        // // $articles = $query->offset($pagination->4)
        // //     ->limit($pagination->limit)
        // //     ->all();
        // //     print_r($articles);
        // // $data = TypeProduct::getPostId($id);
        return $this->render('type_post',
            [
                'data' => $data,
                'pagination' => $pagination,
            ]
        );
    }
    public function actionIndex() {
        // $query = Article::find()->where(['status' => 1]);
        // get the total number of articles (but do not fetch the article data yet)$id = $id = Yii::$app->request->get('id');
        // $id = Yii::$app->request->get('id');
        $data = Post::getPost();
        $count = count($data);
        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);
        // print_r($pagination);
        // die();
        // // limit the query using the pagination and retrieve the articles
        // // $articles = $query->offset($pagination->4)
        // //     ->limit($pagination->limit)
        // //     ->all();
        // //     print_r($articles);
        // // $data = TypeProduct::getPostId($id);
        return $this->render('index',
            [
                'data' => $data,
                'pagination' => $pagination,
            ]
        );
    }
}
