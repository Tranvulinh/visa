<?php
namespace frontend\controllers;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Url;
use common\models\Setting;
use common\models\Post;
use common\models\TypeProduct;
use common\models\SubTypeProduct;
use common\models\TypePost;
use common\models\Product;
use common\models\Slider;
use common\models\Consultant;
use common\models\CustomeFell;
use common\models\InfoMail;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function init()
    {
        parent::init();
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->view->title = "Dịch Vụ Visa Uy Tín"; // Title
        $slider = Slider::getSlider(); // Lấy tất cả SLider 
        $typeProductQT = SubTypeProduct::getSubTypeProductIndex(13); // Lấy visa quốc tế
        $typeProductIndexOT = SubTypeProduct::getSubTypeProductIndex(20); // Lấy dịch vụ khác
        $typePostStatus = TypePost::getAllTypePostStatus();
        $getPost = Post::getPost();
        $getConsultant = Consultant::getConsultant();
        $getCustomeFell = CustomeFell::getCustomeFell();
        // print_r( $getConsultant['']);
        // die();
        return $this->render('index',
            [
                'typeProductQT' => $typeProductQT,
                'typeProductIndexOT' => $typeProductIndexOT,
                'slider' => $slider,
                'typePostStatus' => $typePostStatus,
                'getPost' => $getPost,
                'getConsultant' => $getConsultant,
                'getCustomeFell' => $getCustomeFell,
            ]
        );
    }

    public function actionLocation($lang)
    {
        if (in_array($lang, Yii::$app->params['supportedLanguages'])) {


            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => $lang,
                // 'expire' => time() + Yii::$app->params['cookie.language.expire'],
            ]));

            Yii::$app->language = $lang;
            $backUrl = Yii::$app->request->referrer ? Yii::$app->request->referrer : '/';

            return $this->redirect($backUrl);
        }
    }
    public function actionSendmail(){
        if(Yii::$app->request->get('email')) {
            $email = Yii::$app->request->get('email');
            //lưu thông tin khách hàng
            $dataForm = Yii::$app->request->get();
            $save = InfoMail::updateInfoMail($dataForm);
            if($save) {
                Yii::$app->mailer->compose('mailContent',['model' => Yii::$app->request->get()])
                ->setFrom('fptplayboxfshare@gmail.com')
                ->setTo($email)
                ->setSubject('Gửi yêu cầu nhận thông tin')
                ->send();
                // Gửi mail cho admin
                Yii::$app->mailer->compose('mailAdmin',['model' => Yii::$app->request->get()])
                ->setFrom('fptplayboxfshare@gmail.com')
                ->setTo('tranvulinh280694@gmail.com')
                ->setSubject('Thông Tin Khách Hàng')
                ->send();
                Yii::$app->session->setFlash('success', "Đã gửi mail thành công");
                // return $this->redirect(Url::to('@web/'));
                return $this->redirect(Yii::$app->request->referrer);
            }
            // Yii::$app->session->setFlash('error', "Gửi mail không thành công");
            // return $this->redirect(Url::to('@web/'));
            return $this->redirect(Yii::$app->request->referrer);
        } 
        // Yii::$app->session->setFlash('error', "Gửi mail không thành công");
        // return $this->redirect(Url::to('@web/'));
        return $this->redirect(Yii::$app->request->referrer);
    }  
    // public function actionSendmailContact(){
    //     if(Yii::$app->request->get('email')) {
    //         $email = Yii::$app->request->get('email');
    //         Yii::$app->mailer->compose('mailContent',['model' => Yii::$app->request->get()])
    //         ->setFrom('fptplayboxfshare@gmail.com')
    //         ->setTo($email)
    //         ->setSubject('Gửi yêu cầu nhận thông tin')
    //         ->send();
    //         // Gửi mail cho admin
    //         Yii::$app->mailer->compose('mailAdmin',['model' => Yii::$app->request->get()])
    //         ->setFrom('fptplayboxfshare@gmail.com')
    //         ->setTo('tranvulinh280694@gmail.com')
    //         ->setSubject('Thông Tin Khách Hàng')
    //         ->send();
    //         Yii::$app->session->setFlash('success', "Đã gửi mail thành công");
    //         return $this->redirect(Url::to('@web/'));

    //         // Yii::$app->session->setFlash('success', "Đã gửi mail thành công");
    //         // return $this->redirect(Url::to('@web/post/contact'));
    //     } 
    //     Yii::$app->session->setFlash('error', "Gửi mail không thành công");
    //     return $this->redirect(Url::to('@web/'));
    // }  
}
