<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
// use yii\web\UploadedFile;
// use common\models\forms\PostForm;
use common\models\CountryProduct;
use common\models\Slider;
use yii\data\Pagination;
use yii\web\HttpException;

/**
 * Post controller
 */
class CountryProductController extends Controller
{
    
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionCountryProduct() {
        $id = Yii::$app->request->get('id');
        
        $data = CountryProduct::getTab($id);
        $product = CountryProduct::getCountryProductProduct($id);
        $slider = Slider::getSlider();
        // print_r($data);
        // die();
        if(empty($data)){
            throw new HttpException(404, Yii::t('app', 'Page not found'));
        }
        // $dataRelate = CountryProduct::relateProduct($id,$data['tp_id']);
        return $this->render('flag-index',
            [
                'data' => $data,
                'slider' => $slider,
                'product' => $product,
                // 'dataRelate' => $dataRelate,
            ]
        );
    }
    public function actionContent() {
        $id = Yii::$app->request->get('id');
        $data = CountryProduct::getCountryProductId($id);
        return $this->render('content',
            [
                'data' => $data,
                // 'dataRelate' => $dataRelate,
            ]
        );

    }

    
  
        
       
        
}
   
