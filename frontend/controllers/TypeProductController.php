<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
// use yii\web\UploadedFile;
// use common\models\forms\PostForm;
use common\models\TypeProduct;
use yii\data\Pagination;
// use common\models\TypePost;
// use yii\helpers\ArrayHelper;
// use yii\filters\AccessControl;
// use yii\filters\VerbFilter;

/**
 * Post controller
 */
class TypeProductController extends Controller
{
    
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init() {
        parent::init();
    }
    public function actionTypeProduct() {
        
        $id = Yii::$app->request->get('id');
        // die();
        $data = TypeProduct::getProductFollowTypeProduct($id);
        // foreach ($data as $value) {
        //     echo $value['idTP'];
        // }
        // die;
        $count = count($data);

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);
        // print_r($pagination);
        // die();

        // // limit the query using the pagination and retrieve the articles
        // // $articles = $query->offset($pagination->4)
        // //     ->limit($pagination->limit)
        // //     ->all();
        // //     print_r($articles);

       
        // // $data = TypeProduct::getPostId($id);
        return $this->render('type_product',
            [
                'data' => $data,
                'pagination' => $pagination,
            ]
        );
    }
    
    
    
  
        
       
        
}
   
