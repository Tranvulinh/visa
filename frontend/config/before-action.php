<?php

use yii\helpers\Json;
use yii\helpers\ArrayHelper;

return function ($event) {
    $user = null;
    if (!Yii::$app->user->isGuest) {
        $user = Yii::$app->user->identity;
    }

    /*[BEGIN] Set app language*/
    $lang = null;
    if ($user !== null) {
        //Lấy cài đặt ngôn ngữ của user
        $setting = yii\helpers\Json::decode($user->setting);
        $lang = ArrayHelper::getValue($setting, 'language');
    } else {
        $cookies = Yii::$app->request->cookies;
        if (($cookie = $cookies->get('language')) !== null) {
            $lang = $cookie->value;
        } else {
            //Lấy ngôn ngữ ưu thích từ request header 'Accept-Language'
            //VD: $_SERVER['HTTP_ACCEPT_LANGUAGE'] = "vi,en;q=0.9,en-US;q=0.8" => user ưu tiên dùng tiếng Việt
            //Nếu trong 'accept-language' không có 'vi', mặc định sẽ để là tiếng Anh.
            //+  Path: ~\business-fshare\app\config\params.php
            //+  ...'supportedLanguages' => ['en', 'vi'], <- sẽ lấy item đầu tiên trong
            //   trường hợp: 'accept-language': "fr,cn;q=0.9" //Không chứa ngôn ngữ mà Web hỗ trợ
            $preferredLanguage = Yii::$app->request->getPreferredLanguage(Yii::$app->params['supportedLanguages']);
            $lang = $preferredLanguage;

            /* Nhận diện location theo IP của user */ //Todo remove để đo lường hoặc làm sau
            //$ip = Yii::$app->request->getUserIP(); //$_SERVER['REMOTE_ADDR']; //'40.73.255.255'; //'118.69.164.239';
            //$dataArray = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
            //$location = isset($dataArray->geoplugin_countryCode) ? $dataArray->geoplugin_countryCode : 'en';
            //$lang = strcasecmp('VN', $location) == 0 ? 'vi' : 'en';
        }
    }
    if (in_array($lang, Yii::$app->params['supportedLanguages'])) {
        Yii::$app->language = $lang;
    }
    /*[END] Set app language*/
};
