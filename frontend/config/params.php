<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportedLanguages' => ['en', 'vi'],

];
