<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

// use \yii\web\Request;
// $baseUrl = str_replace('/frontend/web', '/', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),

    'language' => 'vi',
    'sourceLanguage' => 'en-US',
    'on beforeAction' => require(__DIR__ . '/before-action.php'),
    'homeUrl' => '/',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            // 'baseUrl' => $baseUrl,
            'class' => 'common\components\Request',
            'web'=> '/frontend/web'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                // 'username' => 'vulinh280694@gmail.com',
                // 'password' => 'gavhoearlvhgprpo',
                'username' => 'fptplayboxfshare@gmail.com',
                'password' => 'yceucupuialpjuxq',
                

                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        // 'request'=>[
        //     'class' => 'common\components\Request',
        //     'web'=> '/frontend/web',
        // ],


        'urlManager' => [
            // 'baseUrl' => $baseUrl,
            // 'class' => 'yii\web\UrlManager',
            // // Disable index.php
            'showScriptName' => false,
            // // Disable r= routes
            'enablePrettyUrl' => true,
            // 'suffix' => '.html',
            'rules' => [
                
                
                [
                    'pattern' => '<idP>/<idT>/<id>',
                    'route' => 'country-product/country-product',
                    'suffix' => '.htm',
                ],
                [
                    'pattern' => '<s_title>.html/<id>',
                    'route' => 'product/detail-product',
                    'suffix' => '',
                ],

                [
                    'pattern' => '<id>',
                    'route' => 'type-product/type-product/',
                    'suffix' => '.html',
                ],
                [
                    'pattern' => '<idT>/<id>',
                    'route' => 'sub-type-product/sub-type-product',
                    'suffix' => '.html',

                    //  'pattern' => '<idE>/<id>',
                    // 'route' => 'sub-type-product/type',
                    // 'suffix' => '.html',
                ],
                
                // '<idA>/<id>' => 'post/detail-post',
                // [
                //     'pattern' => '<idA>/<id>',
                //     'route' => 'post/detail-post',
                //     'suffix' => '.html',
                // ],
                // '<idT>/<id>' => 'sub-type-product/type',
                [
                    'pattern' => '<idT>/<id>',
                    'route' => 'sub-type-product/type',
                    'suffix' => '.h',
                ],
                // [
                //     'pattern' => '<id>',
                //     'route' => 'sub-type-product/index',
                //     'suffix' => '.html',
                // ],
                // 'danh-muc/<idT>/<id>' => ,
                // 'visa/<idT>/<id>' =>  'sub-type-product/sub-type-product',
                // [
                //     'pattern' => 'visa/<id>',
                //     'route' => 'type-product/type-product/',
                //     'suffix' => '.html',
                // ],

                'location/<lang>' => 'site/location',
                /* Chi tiết của bài post [post/type_post/post] */
                '<idT>/<id>' => 'post/detail-post',
                // [
                //     'pattern' => '<idT>/<id>',
                //     'route' => 'post/detail-post',
                //     'suffix' => '.html',
                // ],
                // 've-chung-toi' => 'post/ve-chung-toi',
                'contact' => 'post/contact',
                // '<idA>/<id>' => 'post/detail-post',
                // [
                //     'pattern' => '<idT>/<id>',
                //     'route' => 'post/detail-post',
                //     'suffix' => '.html',
                // ],
                /* Chi tiết bài viết về chúng tôi */
                'home/<id>' => 'post/detail-post',
                [
                    'pattern' => 'home/<id>',
                    'route' => 'post/detail-post',
                    'suffix' => '.html',
                ],
                /* Danh sách tất cả các type_post [post/type_post] */
                '<id>' => 'type-post/type-post'  ,
                [
                    'pattern' => 'post/<id>',
                    'route' => 'type-post/type-post',
                    'suffix' => '',
                ],
                // '<id>.html' => 'type-product/type-product', 
                // [
                //     'pattern' => 'post/<id>',
                //     'route' => 'type-product/type-product',
                //     'suffix' => '',
                // ],
                // 'visa/<id>' => 'type-product/type-product/',

                
                // '<idT>/<id>' =>  'sub-type-product/sub-type-product',
                'gioi-thieu/<idT>/<id>' =>  'sub-type-product/content',
                'danh-sach/<idT>/<id>' =>  'sub-type-product/legation',
                'tai-lieu/<idT>/<id>' =>  'sub-type-product/document',
                'tai-lieu/<idT>/<idS>/<id>' =>  'sub-type-product/document-detail',
                // '<id>/<idT><idP>' => 'country-product/country-product',
                // '<id>/<idT><idP>' => 'country-product/country-product',
                [
                    'pattern' => '<idT>/<idP>/<id>',
                    'route' => 'country-product/country-product',
                    'suffix' => '.htm',
                ],
                
                // 'visa/<idT>/<id>'                => 'sub-type-product/content',
                
                // '<id>' => 'sub-type-product/content',
                
                // 'danh-muc/<idT>/<id>' => 'sub-type-product/index',

                
                   
                
            ],
            // 'scriptUrl'=>'/index.php',
        ],
        // 'urlManagerFrontend' => [
        //     'enablePrettyUrl' => false,

        //     'class' => 'yii\web\UrlManager',

        //     // 'hostInfo' => 'http://localhost/frontend',

        //     'baseUrl' => dirname(dirname(__DIR__)),
        // ],
        
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                // 'username' => 'vulinh280694@gmail.com',
                // 'password' => 'gavhoearlvhgprpo',
                'username' => 'fptplayboxfshare@gmail.com',
                'password' => 'yceucupuialpjuxq',
                

                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        // ...
    
        
    ],
    'params' => $params,
];
