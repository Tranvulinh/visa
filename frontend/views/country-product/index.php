<?php 
use yii\helpers\Url;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>

<div class="page_mainslider">
    <div class="page-banner">
        <img src="<?php echo Url::to('@web/'.$setting['banner_visa']) ?>" alt="" />
    </div>
</div>
<div class="mainChild">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo Url::to('@web') ?>"><?= Yii::t('app', 'Home') ?></a></li>
            <li><a href="<?php echo Url::to(['type-product/type-product','id' => $data['tp_id']]) ?>"><?php echo json_decode($data['tp_name'])->$lang ?></a></li>
            <li class="active"><?php echo json_decode($data['name'])->$lang ?></li>
        </ol>
        <div class="row">
            <div class="col-md-8 box-detail">
                <h1 class="ttl-3"><?php echo json_decode($data['name'])->$lang ?></h1>
                <?php echo json_decode($data['description'])->$lang ?>

            </div>
            <div class="col-md-4">
                <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                <div class="form-style-other">
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Name') ?></label>
                        <input type="text" name="name" class="form-control" placeholder="<?= Yii::t('app', 'Name') ?>" />
                    </div>
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Email') ?></label>
                        <input type="text" nmae="email" class="form-control" placeholder="<?= Yii::t('app', 'Email') ?>" />
                    </div>
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Phone') ?></label>
                        <input type="text" class="form-control" name="phone" placeholder="<?= Yii::t('app', 'Phone') ?>" />
                    </div>
                    <button class="btnSm"><?= Yii::t('app', 'Send') ?></button>
                </div>
                </form>
                <h2 class="ttl-3"><?= Yii::t('app', 'ONLINE SUPPORT') ?></h2>
                <div class="boxSupport">
                    <div class="img">
                        <img src="<?php echo Url::to('@web/img/common/img-support.jpg') ?>" alt="">
                    </div>
                    <h4><?= Yii::t('app', 'Customer Service') ?></h4>
                    <p><?= Yii::t('app', 'Thời gian làm việc : ') ?>8:30 am - 05:30 pm</a></p>
                    <p class="hotline"><?= Yii::t('app', 'Hotline : ') ?><a href="tel:<?php echo $setting['phone'] ?>"><?php echo $setting['phone'] ?></a></p>
                </div>
                <h2 class="ttl-3"><?= Yii::t('app', 'Relate Post') ?></h2>

                <ul class="newList">
                    <?php if(empty($dataRelate)): ?>
                        <div><?= Yii::t('app', 'Nothing to show') ?></div>
                    <?php else : ?>
                        <?php foreach ($dataRelate as $dtr) : ?>
                           
                            
                            <li><a href="<?php echo Url::to(['product/detail-product',
                                'id' => $dtr['title_url'],
                                's_title' => $dtr['tp_title_url']
                            ]) ?>"><?php echo json_decode($dtr['name'])->$lang ?></a></li>
                            

                        <?php endforeach ?>
                        
                    <?php endif ?>
                </ul>

            </div>
        </div>
    </div>
</div>