<?php 
use yii\helpers\Url;
use yii\helpers\StringHelper;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>

    <?php //print_r(json_decode($data['description_en']));die(); ?>
<div class="page">
    
    <div id="slider">
        <?php foreach ($slider as $sd) : ?>
            <div class="bgslider">
                <a href="#">
                    <img class="bgimg" src="<?php echo Url::to('@web/'.$sd['image']) ?>" alt="">
                </a>
            </div>
        <?php endforeach ?>
    </div>
    <div class="mainhome">
        <div class="container">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable"> 
                    <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?> 
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-error alert-dismissable"> 
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?> 
            <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                <div class="form-info">
                    <div class="item">
                        <label><?= Yii::t('app', 'Name') ?></label>
                        <input type="text" name="name" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Email') ?></label>
                        <input type="text" name="email" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Phone') ?></label>
                        <input type="text" name="phone" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <button class="btnSend"><?= Yii::t('app', 'Send') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="mainChild">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<?php echo Url::to('/')  ?>"><?= Yii::t('app', 'Home') ?></a></li>
                    <li><a href="<?php echo Url::to(['type-product/type-product','id' => $data['tp_title_url']]) ?>"><?php echo json_decode($data['tp_name'])->$lang ?></a></li>
                    <li><a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $data['st_title_url'],'idT' => $data['tp_title_url']]) ?>"><?php echo json_decode($data['st_name'])->$lang ?></a></li>
            </ol>
            <div class="row default">
                <div class="col-sm-12">
                    <div id="exTab1" class="container">	
                        <ul  class="nav nav-pills">
                            <?php 
                            if($lang == "vi"){
                                foreach (json_decode($data['description_vi'])->title as $keyVI => $dtVI){
                                $active = ($keyVI == 0) ? "active" : '' ?>

                                <li class="<?php echo $active ?>">
                                    <a  href="#<?php echo $keyVI ?>" data-toggle="tab"><?php echo $dtVI ?></a>
                                </li>
                            <?php }
                            } else {
                                foreach (json_decode($data['description_en'])->title as $keyEN => $dtEN) { 
                                    $active = ($keyEN == 0) ? "active" : '' ?>

                                <li class="<?php echo $active ?>">
                                    <a  href="#<?php echo $keyEN ?>" data-toggle="tab"><?php echo $dtEN ?></a>
                                </li>
                            
                            <?php } 
                            } ?>
                            
                        </ul>
                
                        <div class="tab-content clearfix">
                            <?php 
                            if($lang == "vi"){
                                foreach (json_decode($data['description_vi'])->content as $kVI => $ctVI){ 
                                    $active = ($kVI == "0") ? "active" : ''?>

                                <div class="tab-pane <?php echo $active ?>" id="<?php echo $kVI ?>">
                                    
                                     <div><?php echo $ctVI  ?></div>
                                </div>
                                
                            <?php }
                            } else {
                                foreach (json_decode($data['description_en'])->content as $kEN => $ctEN) {
                                $active = ($kEN == "0") ? "active" : '' ?>

                                 <div class="tab-pane <?php echo $active ?>" id="<?php echo $kEN ?>">
                                    
                                     <div><?php echo $ctEN  ?></div>
                                </div>
                            
                            <?php } 
                            } ?>
                        </div>
                  </div>
                </div>
                <div class="gridNews row">
            <?php foreach ($product as $pd) : ?>
        
            <div class="col-md-4 col-xs-6">
                <div class="item">
                    <a href="<?php echo Url::to(['product/detail-product','id' => $pd['title_url'],'s_title' => $pd['st_title_url']]) ?>"><img src="<?php echo Url::to('@web/'.$pd['image']) ?>" alt="" /></a>
                    <div class="info">
                        <a href="<?php echo Url::to(['product/detail-product','id' => $pd['title_url'],'s_title' => $pd['st_title_url']]) ?>"><?php echo json_decode($pd['name'])->$lang ?></a>
                        <p class="txt"><?php echo json_decode($pd['short_description'])->$lang ?></p>
                        <a href="<?php echo Url::to(['product/detail-product','id' => $pd['title_url'],'s_title' => $pd['st_title_url']]) ?>" class="v-more"> <?= Yii::t('app', 'Details') ?></a>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
           
        </div>
            </div>
        </div>
    </div>

</div>

