 /* custom JS here */
 $(document).ready(function() {
     $('.menu-page').each(function() {
         var currentid = $(this).attr('href');
         $(this).click(function(e) {
             e.preventDefault();
             $(this).toggleClass('active-menu');
             $(currentid).toggleClass('open-sub');
             $('body').toggleClass('open-page');
         });
     });


 });
 $(window).scroll(function() {
     if ($(this).scrollTop()) {
         $('.to-top').css({ bottom: 0 });
     } else {
         $('.to-top').css({ bottom: -100 });
     }
 });
 $('.to-top a').click(function(e) {
     var href = $(this).attr("href"),
         offsetTop = href === "#" ? 0 : $(href).offset().top;
     $('html, body').stop().animate({
         scrollTop: offsetTop
     }, 1000);
     e.preventDefault();
 });
 $('#menu ul.menu li').each(function() {
     if ($(this).find('li').size() > 0) {
         $(this).append('<span class="icon"></span>');
         $(this).addClass('has-sub');
         var subUl = $(this).find('ul');
         subUl.addClass('sub');

     }
 });
 $('span.icon').each(function() {
     $(this).click(function(e) {
         $(this).parent().find('.sub').toggleClass('showchild');
         $(this).toggleClass('open-icon')
     });

 });
 $('#slider').slick({
     infinite: true,
     arrows: true,
     autoplay: true,
     autoplaySpeed: 5000
         //dots: true
 });
 $('.sliderItem').slick({
    infinite: true,
    slidesToShow: 3,
    prevArrow: '<a class="arrow arrow--prev" href="#"><i class="fa fa-chevron-circle-left"></i></a>',
    nextArrow: '<a class="arrow arrow--next" href="#"><i class="fa fa-chevron-circle-right"></i></a>',
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
  });

 /*Menu child have sub*/
 $('.menuchild li.has-sub>a').on('click', function(event) {
     var element = $(this).parent('li');
     if (element.hasClass('open')) {
         element.removeClass('open');
         //element.removeClass('active');
         element.find('li').removeClass('open');
         element.find('ul').slideUp();
     } else {
         element.addClass('open');
         //element.addClass('active');
         element.children('ul').slideDown();
         element.siblings('li').children('ul').slideUp();
         element.siblings('li').removeClass('open');
         element.siblings('li').find('li').removeClass('open');
         element.siblings('li').find('ul').slideUp();
     }
 });
 $("input[placeholder]").focusin(function() {
         $(this).data('place-holder-text', $(this).attr('placeholder')).attr('placeholder', '');
     })
     .focusout(function() {
         $(this).attr('placeholder', $(this).data('place-holder-text'));
});
function waypointEl() {
    var way = $("[data-waypoint]");
    way.each(function () {
        var _el = $(this)
            , _ofset = _el.data("waypoint")
            , _up = _el.data("waypointup");
        _el.waypoint(function (direction) {
            if (direction == "down") {
                _el.addClass("active")
            } else {
                if (_up) {
                    _el.removeClass("active")
                }
            }
        }, {
                offset: _ofset
            })
    })
}
waypointEl() 