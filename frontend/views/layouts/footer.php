<?php use yii\helpers\Url;$setting = Yii::$app->params['setting']; $lang = Yii::$app->language;?>
<footer class="footer">    
    <div class="container">        
        <div class="row">          
            <div class="col-sm-4">                
                <?php echo json_decode($setting['content_footer'])->$lang ?>                
                <div class="share">                    
                <a target="blank" href="<?php echo $setting['link_facebook'] ?>"><i class="fa fa-facebook"></i></a>                    
                <a target="blank" href="<?php echo $setting['link_skype'] ?>"><i class="fa fa-skype"></i></a>                    
                <a target="blank" href="<?php echo $setting['link_facebook'] ?>"><i class="fa fa-whatsapp"></i></a>                    
                <a target="blank" href="<?php echo $setting['link_twitter'] ?>"><i class="fa fa-twitter"></i></a>                
                </div>
            </div>            
            <div class="col-sm-4">
                <h3><?= Yii::t('app', 'CONTACT WITH US') ?></h3>
                <p><?= Yii::t('app', 'Address : ') ?><?php echo $setting['address'] ?> <br />                    
                <?= Yii::t('app', 'Telephone : ') ?><a href="tel:<?php echo $setting['phone'] ?>"><?php echo $setting['phone'] ?></a> <br />                    
                <?= Yii::t('app', 'Email : ') ?><a href="mailto:<?php echo $setting['email'] ?>"><?php echo $setting['email'] ?></a></p>
                <!-- <p class="hotline"><?= Yii::t('app', 'Hotline : ') ?><a href="tel:<?php echo $setting['mobilephone'] ?>"><?php echo $setting['mobilephone'] ?></a></p>          -->
                <div class="hotline-phone-ring-wrap">
                    <div class="hotline-phone-ring">
                        <div class="hotline-phone-ring-circle"></div>
                        <div class="hotline-phone-ring-circle-fill"></div>
                        <div class="hotline-phone-ring-img-circle">
                        <a href="tel:<?php echo $setting['mobilephone'] ?>" class="pps-btn-img">
                            <img src="https://nguyenhung.net/wp-content/uploads/2019/05/icon-call-nh.png" alt="Gọi điện thoại" width="50">
                        </a>
                        </div>
                    </div>
                    <div class="hotline-bar">
                        <a href="tel:<?php echo $setting['mobilephone'] ?>">
                            <span class="text-hotline"><?php echo $setting['mobilephone'] ?></span>
                        </a>
                    </div>
                </div>
                <div class="images d-flex">
                    <a href="#" class="congthuong-img"><img src="<?php echo Url::to('@web/img/common/cong-thuong.png') ?>" alt="Chứng nhận từ bộ công thương"></a>
                    <a class="dmca-badge" href="#"><img src="<?php echo Url::to('@web/img/common/dmca.png') ?>" alt="DMCA.com Protection Status"></a>

                </div>
            </div>            
            <div class="col-sm-4">                
                <?php echo $setting['link_map'] ?>            
            </div>        
        </div>
        <div class="row copyright-wrapper">
            <div class="col-sm-12">
                <div class="copyright">
                    Copyright &copy; Visa Kali 2019
                </div>
            </div>
        </div>  
    </div>
        
    </div>
    <div class="hotline-phone-ring-wrap">
        <div class="hotline-phone-ring">
            <div class="hotline-phone-ring-circle"></div>
            <div class="hotline-phone-ring-circle-fill"></div>
            <div class="hotline-phone-ring-img-circle">
            <a href="tel:<?php echo $setting['mobilephone'] ?>" class="pps-btn-img">
                <img src="https://nguyenhung.net/wp-content/uploads/2019/05/icon-call-nh.png" alt="Gọi điện thoại" width="50">
            </a>
            </div>
        </div>
        <div class="hotline-bar">
            <a href="tel:<?php echo $setting['mobilephone'] ?>">
                <span class="text-hotline"><?php echo $setting['mobilephone'] ?></span>
            </a>
        </div>
    </div>
</footer>
