<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags-->
    <meta name="description" content="Với đội ngũ nhân viên nhiệt tình, chuyên nghiệp và có nhiều năm kinh nghiệm làm về Visa các nước và các dịch vụ du lịch trong và ngoài nước. Chúng tôi cam kết mang đến những dịch vụ nhanh nhất, trung thực nhất, hiệu quả nhất, ít rủi ro nhất với chi phí thấp nhất.">
    <meta name="author" content="">
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Trang Chủ &ndash; Công Ty TNHH Thương Mại và Dịch Vụ Kali" />
    <meta property="og:description" content="Chuyên gia hàng đầu đề ra giải pháp thị thực (VISA). Kinh nghiệm tư vấn cùng sự am hiểu sâu sắc thủ tục với hơn 190 nước trên thế giới." />
    <meta property="og:url" content="https://kalivisa.com/" />
    <meta property="og:site_name" content="kalivisa" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="Trang Chủ &ndash; Công Ty TNHH Thương Mại và Dịch Vụ Kali" />
    <link rel="canonical" href="https://kalivisa.com/" />
    <link rel='shortlink' href='https://kalivisa.com/' />
    <link rel="alternate" type="application/json+oembed" href="" />
    <link rel="alternate" type="text/xml+oembed" href="" />
    <link rel="alternate" href="https://kalivisa.com/" hreflang="vi" />
    <link rel="alternate" href="https://kalivisa.com/" hreflang="en" />
    <meta name="generator" content=""/>
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="favicon.ico" rel="icon">
    <link
        href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&display=swap&subset=vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- <title> Dịch vụ visa uy tín</title> -->
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150110099-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-150110099-1');
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="page">
    <?php echo $this->render('header') ?>
    <?= $content ?>
   
</div>

<?php echo $this->render('footer') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
