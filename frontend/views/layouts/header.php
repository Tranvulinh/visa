<?php 
use yii\helpers\Url;
use yii\helpers\Html;
//biến chung hiển thị tất cả trang
$setting = Yii::$app->params['setting'] ;
$postMenu = Yii::$app->params['postMenu'] ;
$typeProductMenu = Yii::$app->params['typeProductMenu'] ;
$subTypeProductMenu = Yii::$app->params['subTypeProductMenu'] ;
$typePostMenu = Yii::$app->params['typePostMenu'];
//biến ngôn ngữ [setup before-action]
$lang = Yii::$app->language;

?>
<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="111430716923784">
      </div>

<header class="header">
    <div class="container">
        <div class="logo">
            <a href="<?php  echo Url::to('@web/') ?>"><img src="<?php echo Url::to('@web/'.$setting['logo_image'])  ?>" alt=""></a>
        </div>
        <div class="navRight">
            <!-- <form action="<?php  echo Url::to('@web/product/search') ?>">
                <div class="boxSearch sm">
                    <input type="text" name="search" placeholder="<?= Yii::t('app', 'Search .....') ?>" class="form-control" />
                    <button class="btn-search"><i class="fa fa-search"></i></button>
                </div>
            </form> -->
            
            
            <div class="boxAdd">
                <div class="boxAddItem">
                    <i class="fa fa-map-marker"></i><span class="first-child"><?= Yii::t('app', 'Address : ') ?></span><span><?php echo $setting['address'] ?></span>
                </div>
                <div class="boxAddItem">
                    <i class="fa fa-phone"></i>
                    <span class="first-child"><?= Yii::t('app', 'Telephone : ') ?></span>
                    <a href="tel:<?php echo $setting['phone'] ?>">
                        <?php echo $setting['phone'] ?>
                    </a>
                </div>
            </div>
            <div class="boxLang">
                <a href="<?php echo Url::to(['@web/site/location','lang' => 'vi'])?>"><img src="<?php echo Url::to('@web/img/common/flag-1.png') ?>" alt=""></a>
                <a href="<?php echo Url::to(['@web/site/location','lang' => 'en'])?>"><img src="<?php echo Url::to('@web/img/common/flag-2.png') ?>" alt=""></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="wrapmenu"><a class="menu-page btn-menu" href="#menu"><span
                    class="burger-icon-1"></span><span class="burger-icon-2"></span><span
                    class="burger-icon-3"></span></a>
            <div class="mainmenu" id="menu">
                <ul class="menu">
                    <?php if(!empty($postMenu)) : ?>
                    <li><a class="home" href="<?php  echo Url::to('@web/') ?>"><i class="fa fa-home"></i></a></li>
                    <!-- <li><a href="<?php  echo Url::to('@web/') ?>"><i class="fa fa-home"></i><?= Yii::t('app', 'Home') ?></a></li> -->
                    <li><a href="<?php echo Url::to(['post/detail-post','id' => $postMenu['title_url']]) ?>"><?php echo json_decode($postMenu['name'])->$lang?></a></li>
                    <?php endif ?>
                    <?php foreach ($typeProductMenu as $tpm) : ?>
                    <li><a href="<?php echo Url::to(['type-product/type-product','id' => $tpm['title_url']])  ?>"><?php echo json_decode($tpm['name'])->$lang ?></a>
                        <ul class="menuChild">
                            
                            <?php foreach ($subTypeProductMenu as $pm) : ?>
                                <?php if($tpm['id'] == $pm['id_type_product']) : ?>
                                    <?php 
                                    $array = [12,20];
                                    if(in_array($pm['id_type_product'],$array)) { ?>
                                        <li><a href="<?php echo Url::to(['sub-type-product/type','id' => $pm['title_url'],'idT' => $pm['tp_title_url']]) ?>"><?php echo json_decode($pm['name'])->$lang ?></a></li>
                                    <?php } 
                                    else {?>
                                        <li><a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $pm['title_url'],'idT' => $pm['tp_title_url']]) ?>"><?php echo json_decode($pm['name'])->$lang ?></a></li>
                                    <?php } ?>
                                <?php endif ?>
                            <?php endforeach ?>     
                        </ul>
                    </li>
                    <?php endforeach ?>
                    
                    
                    <li><a href="#"><?= Yii::t('app', 'Post') ?></a>
                        <ul class="menuChild">
                            <?php foreach ($typePostMenu as $tp) :?>
                                    
                                    <li><a href="<?php echo Url::to(['type-post/type-post', 'id' => $tp['title_url']]) ?>"><?php echo json_decode($tp['name'])->$lang ?></a></li>
                                
                            <?php endforeach ?>    
                        </ul>
                    </li>
                    <li><a href="<?php echo Url::to(['post/contact'])  ?>"><?= Yii::t('app', 'Contact') ?></a></li>
                    <li class="search">
                        <a class="btn-search navbar-search-btn md" href="#"><i class="icon_search"></i></a>
                        <form action="<?php  echo Url::to('@web/product/search') ?>">
                            <div class="boxSearch navbar-search">
                                <input type="text" name="search" placeholder="<?= Yii::t('app', 'Search .....') ?>" class="form-control" />
                                <button class="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>

                </ul>
                <!-- <form action="<?php  echo Url::to('@web/product/search') ?>">
                    <div class="boxSearch">
                        <input type="text" name="search" placeholder="<?= Yii::t('app', 'Search .....') ?>" class="form-control" />
                        <button class="btn-search"><i class="fa fa-search"></i></button>
                    </div>
                </form> -->
                <div class="boxLang sm">
                    <a href="<?php echo Url::to(['@web/site/location','lang' => 'vi'])?>"><img src="<?php echo Url::to('@web/img/common/flag-1.png') ?>" alt=""></a>
                    <a href="<?php echo Url::to(['@web/site/location','lang' => 'en'])?>"><img src="<?php echo Url::to('@web/img/common/flag-2.png') ?>" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</header>
