<?php 
use yii\helpers\Url;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>

<div id="slider">
        <?php foreach ($slider as $sd) : ?>
            <div class="bgslider">
                <a href="#">
                    <img class="bgimg" src="<?php echo Url::to('@web/'.$sd['image']) ?>" alt="">
                </a>
            </div>
        <?php endforeach ?>
    </div>
    <div class="mainhome">
        <div class="container">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable"> 
                    <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?> 
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-error alert-dismissable"> 
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?> 
            <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                <div class="form-info">
                    <div class="item">
                        <label><?= Yii::t('app', 'Name') ?></label>
                        <input type="text" name="name" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Email') ?></label>
                        <input type="text" name="email" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Phone') ?></label>
                        <input type="text" name="phone" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <button class="btnSend"><?= Yii::t('app', 'Send') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="mainChild">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo Url::to('@web') ?>"><?= Yii::t('app', 'Home') ?></a></li>
            <li><a href="<?php echo Url::to(['type-product/type-product','id' => $data['tp_title_url']]) ?>"><?php echo json_decode($data['tp_name'])->$lang ?></a></li>
            
            

            <?php 
            $array = [12,20];
            if(in_array($data['id_type_product'],$array)) { ?>
                <li><a href="<?php echo Url::to(['sub-type-product/type','id' => $data['st_title_url'],'idT' => $data['tp_title_url']]) ?>"><?php echo json_decode($data['st_name'])->$lang ?></a></li>
            <?php } 
            else {?>
                <li><a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $data['st_title_url'],'idT' => $data['tp_title_url']]) ?>"><?php echo json_decode($data['st_name'])->$lang ?></a></li>
            <?php } ?>



        </ol>
        <div class="row">
            <div class="col-md-8 box-detail">
                <h1 class="ttl-3"><?php echo json_decode($data['name'])->$lang ?></h1>
                <?php echo json_decode($data['description'])->$lang ?>

            </div>
            <div class="col-md-4">
                <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                <div class="form-style-other">
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Name') ?></label>
                        <input type="text" name="name" class="form-control" placeholder="<?= Yii::t('app', 'Name') ?>" />
                    </div>
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Email') ?></label>
                        <input type="text" nmae="email" class="form-control" placeholder="<?= Yii::t('app', 'Email') ?>" />
                    </div>
                    <div class="form-style-item">
                        <label><?= Yii::t('app', 'Phone') ?></label>
                        <input type="text" class="form-control" name="phone" placeholder="<?= Yii::t('app', 'Phone') ?>" />
                    </div>
                    <button class="btnSm"><?= Yii::t('app', 'Send') ?></button>
                </div>
                </form>
                <h2 class="ttl-3"><?= Yii::t('app', 'ONLINE SUPPORT') ?></h2>
                <div class="boxSupport">
                    <div class="img">
                        <img src="<?php echo Url::to('@web/img/common/img-support.jpg') ?>" alt="">
                    </div>
                    <h4><?= Yii::t('app', 'Customer Service') ?></h4>
                    <p><?= Yii::t('app', 'Thời gian làm việc : ') ?><?php echo $setting['work_time'] ?></a></p>
                    <p class="hotline"><?= Yii::t('app', 'Hotline : ') ?><a href="tel:<?php echo $setting['mobilephone'] ?>"><?php echo $setting['mobilephone'] ?></a></p>
                </div>
                <h2 class="ttl-3"><?= Yii::t('app', 'Relate Post') ?></h2>

                
            </div>
        </div>
    </div>
</div>