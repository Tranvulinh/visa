<?php 
use yii\helpers\Url;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
$lang = Yii::$app->language;
?>
<?php //print json_decode(Yii::$app->params['setting']['content_footer'])->vi;die(); ?>
<style>
    .alert-success{
        margin-bottom: 50px;
    }
    .sNews img {
    	width:100%;
    	border-radius: 100%;
    }
    .sNews .title i {
        color:red;
    }
    .sNews .title {
        padding-top:10px;
    }
</style>
<div class="page_mainslider">
    <div id="slider">
        <?php foreach ($slider as $sd) : ?>
            <div class="bgslider">
                <a href="<?php echo $sd['link'] ?>" target="_blank">
                    <img class="bgimg" src="<?php echo Url::to('@web/'.$sd['image']) ?>" alt="">
                </a>
            </div>
        <?php endforeach ?>
    </div>
    <div class="mainhome">
        <div class="container">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable"> 
                    <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?> 
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-error alert-dismissable"> 
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?> 
            <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                <div class="form-info">
                    <div class="item">
                        <label><?= Yii::t('app', 'Name') ?></label>
                        <input type="text" name="name" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Email') ?></label>
                        <input type="text" name="email" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <label><?= Yii::t('app', 'Phone') ?></label>
                        <input type="text" name="phone" class="form-control" placeholder="" />
                    </div>
                    <div class="item">
                        <button class="btnSend"><?= Yii::t('app', 'Send') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<section class="sWhy" data-waypoint="80%">
    <div class="container">
        <h2 class="ttl"><?= Yii::t('app', 'WHY CHOOSE US') ?></h2>
        <ul>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-1.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'HANDLING<br>FAST PROFILE') ?></h3>
            </li>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-2.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'Free<br>consultation') ?></h3>
            </li>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-3.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'PERCENTAGE RATE <br> 99%') ?></h3>
            </li>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-4.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'Cost <br> savings') ?></h3>
            </li>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-5.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'RESPONSIBILITY TO <br>CUSTOMER') ?></h3>
            </li>
            <li>
                <div class="icon"><img src="<?php echo Url::to('@web/img/common/ic-6.png') ?>" alt=""></div>
                <h3><?= Yii::t('app', 'PROCEDURE<br>PROFESSION') ?></h3>
            </li>
        </ul>
    </div>
</section>
<section class="ref" data-waypoint="80%">
    <div class="container">
        <h2 class="ttl">Cố vấn cấp cao Kali Visa</h2>
        <div class="d-flex">
        <?php foreach ($getConsultant as $gc) : ?>
        
            <div class="ref-item">
                <div class="avatar">
                    <img src="<?php echo Url::to('@web/'.$gc['image']) ?>" alt="">
                </div>
                <div class="info">
                    <p class="name"><h3><?php echo $gc['name'] ?></h3></p>
                    <small><?php echo json_decode($gc['job'])->$lang ?></small>
                    <p class="position"><?php echo json_decode($gc['description'])->$lang ?></p>
                </div>
            </div>
           <?php endforeach?>
        </div>
        
    </div>
</section>
<section class="sTour" data-waypoint="80%">
    <div class="container">
        <h2 class="ttl"><?= Yii::t('app', 'SERVICES ARE INTERESTED') ?></h2>
        <div class="gridTour">
            <?php foreach ($typeProductQT as $tp) : ?>
            <div class="gridTourItem">
                <div class="gridTourWrap">
                    <div class="gridTourImg">
                        <a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $tp['title_url'], 'idT' => $tp['t_title_url']])  ?>"><img src="<?php echo Url::to('@web/'.$tp['image']) ?>" alt=""></a>
                    </div>
                    <div class="gridTourInfo">
                        <a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $tp['title_url'], 'idT' => $tp['t_title_url']])  ?>"></a>
                        <h3><?php echo json_decode($tp['name'])->$lang ?></h3>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
            <?php foreach ($typeProductIndexOT as $tpo) : ?>
            <div class="gridTourItem">
                <div class="gridTourWrap">
                    <div class="gridTourImg">
                        <a href="<?php echo Url::to(['sub-type-product/type','id' => $tpo['title_url'] , 'idT' => $tpo['t_title_url']])  ?>"><img src="<?php echo Url::to('@web/'.$tpo['image']) ?>" alt=""></a>
                    </div>
                    <div class="gridTourInfo">
                        <a href="<?php echo Url::to(['sub-type-product/type','id' => $tpo['title_url'], 'idT' => $tpo['t_title_url']])  ?>"></a>
                        <h3><?php echo json_decode($tpo['name'])->$lang ?></h3>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<section class="sNews" data-waypoint="80%">
    <div class="container">
        <h2 class="ttl"><?= Yii::t('app', 'Customer Review') ?></h2>
        <div class="row">
            <!-- <div class="sliderItem"> -->
            	<?php foreach ($getCustomeFell as $gp) : ?>
        		<div class="col-sm-4">
        			<div class="row">
        				<div class="col-sm-4">
        					<img src="<?php echo Url::to('@web/'.$gp['image']) ?>" alt="" />
        				</div>
        				<div class="col-sm-8">
        					<div class="info">
                           
                                <p class="txt"><?php echo json_decode($gp['description'])->$lang ?></p>
                                
                            </div>
        				</div>

        			</div>
        			<div class="title">
                        <?php echo json_decode($gp['name'])->$lang ?>
                            
                    </div>
        			<div class="title">
                        <i><?php echo json_decode($gp['company'])->$lang ?></i>
                    </div>
                	
                	
                </div>
                <?php endforeach ?>
            <!-- </div> -->
        </div> 
        
    </div>
</section>
<!-- <div class="gridTourItem">
                
                    
                        
                    <div class="item">
                        </a>
                        <div class="info">
                            <div class="title"><?php echo json_decode($gp['name'])->$lang ?></div>
                            
                            <p class="txt"><?php echo json_decode($gp['description'])->$lang ?></p>
                            
                        </div>
                    </div>
                    
                
            </div> -->