<?php 
use yii\helpers\Url;
use yii\widgets\LinkPager;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>
<?php ?>
<div class="page_mainslider">
    <div class="page-banner">
        <img src="<?php echo Url::to('@web/'.$setting['banner_visa']) ?>" alt="" />
    </div>
</div>
<div class="mainChild">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo Url::to('/')  ?>"><?= Yii::t('app', 'Home') ?></a></li>
            
        </ol>
        <div class="gridNews row">
            <?php foreach ($data as $dt) : ?>
            <div class="col-md-4 col-xs-6">
                <div class="item">
                    <a href="<?php echo Url::to(['post/detail-post','id' => $dt['id'],'title' => json_decode($dt['name'])->$lang ]) ?>"><img src="<?php echo Url::to('@web/'.$dt['image']) ?>" alt="" /></a>
                    <div class="info">
                        <a href="<?php echo Url::to(['post/detail-post','id' => $dt['id'],'title' => json_decode($dt['name'])->$lang]) ?>"><?php echo json_decode($dt['name'])->$lang ?></a>
                        <p class="txt"><?php echo json_decode($dt['short_description'])->$lang ?></p>
                        <a href="<?php echo Url::to(['post/detail-post','id' => $dt['id'],'title' => json_decode($dt['name'])->$lang]) ?>" class="v-more"> <?= Yii::t('app', 'Details') ?></a>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <div class="text-center">
            <?php 
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                ?>
        </div>
    </div>
</div>