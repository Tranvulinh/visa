<?php 
use yii\helpers\Url;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>
<div class="page_mainslider">
    <div class="page-banner">
        <img src="<?php echo Url::to('@web/'.$setting['banner_post']) ?>" alt="" />
    </div>
</div>
<div class="mainChild">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#"> <?= Yii::t('app', 'Home') ?></a></li>
            <li class="active"><?= Yii::t('app', 'Contact') ?></li>
        </ol>
        <div class="row">
            <div class="col-md-8">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable"> 
                        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?> 
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-error alert-dismissable"> 
                        <?= Yii::$app->session->getFlash('error') ?>
                    </div>
                <?php endif; ?> 
                <h1 class="ttl-3"><?= Yii::t('app', 'CONTACT WITH US') ?></h1>
                <form action="<?php echo Url::to('@web/site/sendmail') ?>" method="GET">
                    <div class="form-style">
                        <div class="row">
                            <div class="col-md-6">
                                <label><?= Yii::t('app', 'Name') ?></label>
                                <input type="text" name="name" class="form-control" placeholder="<?= Yii::t('app', 'Name') ?>" />
                            </div>
                            <div class="col-md-6">
                                <label><?= Yii::t('app', 'Phone') ?></label>
                                <input type="text" name="phone" class="form-control" placeholder="<?= Yii::t('app', 'Phone') ?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label><?= Yii::t('app', 'Email') ?></label>
                                <input type="text" name="email" class="form-control" placeholder="<?= Yii::t('app', 'Email') ?>" />
                            </div>
                            <div class="col-md-6">
                                <label><?= Yii::t('app', 'Address') ?></label>
                                <input type="text" name="address" class="form-control" placeholder="<?= Yii::t('app', 'Address') ?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label><?= Yii::t('app', 'Message') ?></label>
                                <textarea name="message" class="form-control" placeholder="<?= Yii::t('app', 'Message') ?>"></textarea>
                            </div>
                        </div>
                        <button class="btnSm"><?= Yii::t('app', 'Send') ?></button>
                    </div>
                </form>
            </div>
            <div class="col-md-4 addInfo">
                <h2 class="ttl-3"><?= Yii::t('app', 'CONTACT WITH US') ?></h2>
                <address>
                    <p><?php echo json_decode($setting['content_contact'])->$lang ?></p>
                </address>
                <table>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon1.png') ?>" alt="">
                            
                        </td>
                        <td>
                            <?php echo $setting['address'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon2.png') ?>" alt="">
                            
                        </td>
                        <td>
                            <a href="tel:<?php echo $setting['phone'] ?>"><?php echo $setting['phone'] ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon3.png') ?>" alt="">

                            <!-- <img src="img/icon3.png" alt="" /> -->
                        </td>
                        <td>
                            <a href="mailto:<?php echo $setting['email'] ?>"><?php echo $setting['email'] ?></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>