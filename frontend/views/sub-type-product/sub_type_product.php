<?php 
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;

$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>
<?php ?>
<div class="page_mainslider">
    <div class="page-banner">
        <img src="<?php echo Url::to('@web/'.$setting['banner_visa']) ?>" alt="" />
    </div>
</div>
<div class="mainChild">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo Url::to('/')  ?>"><?= Yii::t('app', 'Home') ?></a></li>
            <?php foreach ($data as $d) : ?>
            <li><a href="<?php echo Url::to(['type-product/type-product','id' => $d['tp_title_url']]) ?>"><?php echo json_decode($d['tp_name'])->$lang ?></a></li>
            <li class="active"><?php echo json_decode($d['st_name'])->$lang ?></a></li>
            <?php break;endforeach ?>
        </ol>
        <div class="gridNews row">
            <?php foreach ($data as $dt) : ?>
        
            <div class="col-md-4 col-xs-6">
                <div class="item">
                    <a href="<?php echo Url::to(['product/detail-product','id' => $dt['title_url'],'s_title' => $dt['st_title_url']]) ?>"><img src="<?php echo Url::to('@web/'.$dt['image']) ?>" alt="" /></a>
                    <div class="info">
                        <a href="<?php echo Url::to(['product/detail-product','id' => $dt['title_url'],'s_title' => $dt['st_title_url']]) ?>"><?php echo json_decode($dt['name'])->$lang ?></a>
                        <p class="txt"><?php echo json_decode($dt['short_description'])->$lang ?></p>
                        <a href="<?php echo Url::to(['product/detail-product','id' => $dt['title_url'],'s_title' => $dt['st_title_url']]) ?>" class="v-more"> <?= Yii::t('app', 'Details') ?></a>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
           
        </div>
        

    </div>
</div>
