<?php 
use yii\helpers\Url;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
// print_r($data);
// die;
?>

<body>
    <div class="page">
       
         <div class="page_mainslider">
            <div class="page-banner">
                <img src="<?php echo Url::to('@web/'.$setting['banner_post']) ?>" alt="" />
            </div>
        </div>
        <div class="mainChild">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="<?php echo Url::to('/')  ?>"><?= Yii::t('app', 'Home') ?></a></li>
                    <?php foreach ($data as $d) : ?>
                        
                        
                        <li><a href="<?php echo Url::to(['type-product/type-product','id' => $d['tp_title_url']]) ?>"><?php echo json_decode($d['tp_name'])->$lang ?></a></li>
                        <li><a href="<?php echo Url::to(['sub-type-product/sub-type-product','id' => $d['st_titlle_url'],'idT' => $d['tp_title_url']]) ?>"><?php echo json_decode($d['st_name'])->$lang ?></a></li>
                    <?php break;endforeach ?>
                    
                </ol>
                <div class="row">
                    <div class="col-md-8">
                        <?php foreach ($data as $dt) { ?>
                           
                        <div class="document-item">
                            <a href="<?php echo Url::to(['sub-type-product/document-detail','id' => $dt['title_url'],'idT' => $dt['tp_title_url'],'idS' => $dt['st_titlle_url']]) ?>">
                                <h2 class="title-document"><?php echo json_decode($dt['name'])->$lang ?></h2>
                            </a>
                        </div>
                         
                        <?php } ?>
                        
                    </div>
                    <div class="col-md-4 addInfo">
                <h2 class="ttl-3"><?= Yii::t('app', 'CONTACT WITH US') ?></h2>
                <address>
                    <p><?php echo json_decode($setting['content_contact'])->$lang ?></p>
                </address>
                <table>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon1.png') ?>" alt="">
                            
                        </td>
                        <td>
                            <?= Yii::t('app', 'Address : ') ?><?php echo $setting['address'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon2.png') ?>" alt="">
                            
                        </td>
                        <td>
                            <a href="tel:<?php echo $setting['phone'] ?>"><?php echo $setting['phone'] ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="<?php echo Url::to('@web/img/common/icon3.png') ?>" alt="">

                            <!-- <img src="img/icon3.png" alt="" /> -->
                        </td>
                        <td>
                            <a href="mailto:<?php echo $setting['email'] ?>"><?php echo $setting['email'] ?></a>
                        </td>
                    </tr>
                </table>
            </div>
                </div>
            </div>
        </div>

    </div>


</body>

</html>