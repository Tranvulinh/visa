<?php 
use yii\helpers\Url;
$setting = Yii::$app->params['setting'];
$lang = Yii::$app->language;
?>
<div class="page_mainslider">
    <div class="page-banner">
        <img src="<?php echo Url::to('@web/'.$setting['banner_visa']) ?>" alt="" />
    </div>
</div>
<body>
    <div class="page">
        <div class="mainChild">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="<?php echo Url::to('/')  ?>"><?= Yii::t('app', 'Home') ?></a></li>
                    <li>
                        <a href="<?php echo Url::to(['type-product/type-product','id' => $data['tp_title_url']])  ?>">
                            <?php echo json_decode($data['tp_name'])->$lang ?>
                        </a>
                    </li>
                    <li>
                        <?php echo json_decode($data['name'])->$lang ?>
                    </li>
                </ol>
                <div class="row default">
                    <div class="col-sm-4 default-item col-xs-4">
                        <?php //foreach ($data as $dt) : ?>
                        <a href="<?php echo Url::to(['sub-type-product/content','id' => $data['title_url'],'idT' => $data['tp_title_url']]) ?>">
                            <img src="<?php echo Url::to('@web/img/common/icon4.png') ?>" alt="Giới thiệu">
                            <span>Giới thiệu</span>
                        </a>
                        <?php //endforeach ?>
                    </div>
                    <div class="col-sm-4 default-item col-xs-4">
                        <a href="<?php echo Url::to(['sub-type-product/legation','id' => $data['title_url'],'idT' => $data['tp_title_url']]) ?>">
                            <img src="<?php echo Url::to('@web/img/common/icon5.png') ?>" alt="Danh sách đại sứ quán">
                            <span>Danh sách đại sứ quán</span>
                        </a>
                    </div>
                    <div class="col-sm-4 default-item col-xs-4">
                        <a href="<?php echo Url::to(['sub-type-product/document','id' => $data['title_url'],'idT' => $data['tp_title_url']]) ?>">
                            <img src="<?php echo Url::to('@web/img/common/icon6.png') ?>" alt="Tài liệu hướng dẫn">
                            <span>Tài liệu hướng dẫn</span>
                        </a>
                    </div>
                </div>
                <div class="row default flag-list">
                    <?php foreach ($country as $ct) { ?>
                    <div class="col-sm-3 col-xs-6 default-item">
                        <a href="<?php echo Url::to(['country-product/country-product','id' => $ct['title_url'],'idT' => $ct['st_title_url'], 'idP' =>$ct['tp_title_url']]) ?>">
                            <img src="<?php echo Url::to('@web/'.$ct['image']) ?>" alt="<?php echo $ct['image'] ?>">
                            <span><?php echo json_decode($ct['name'])->$lang ?></span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>