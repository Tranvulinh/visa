
$(document).ready(function() {
    $( "#sortable" ).sortable({
        update: function (event, ui) {
            // console.log();
            length = $(this).children().length;
            $(this).children().each(function (index) {

                if ($(this).attr('data-position') != (length -index)) {
                    $(this).attr('data-position',(length -index)).addClass('updated');
                }
           });
           url = $('#sortable tr').attr('data-url');
           changePosition(url);
       }
    });



    function changePosition(url) {
        var positions = [];
        $('.updated').each(function () {
           positions.push([$(this).attr('data-id'), $(this).attr('data-position')]);
           $(this).removeClass('updated');
        });
        // console.log(positions);
        $.ajax({
            url    : url,
            type   : 'GET',
            dataType: "text",
            // data   : {startPosition:startPosition,startId:startId,newPosition:newPosition,newId:newId},
            data   : {positions:positions},
            success: function (data) 
            {   
                console.log(data);
            }
        })
       
        // console.log(positions);

        // $.ajax({
        //    url: url,
        //    method: 'GET',
        //    dataType: 'text',
        //    data: {
        //        update: 1,
        //        positions: positions
        //    }, success: function (response) {
        //         console.log(response);
        //    }
        // });
    }  

    $( "#sortable" ).disableSelection();
//=======================================================================================
// console.log($('.content_clone').find('.container-items'));
    
    // Nút nhấn thêm content mới
    $('.plusContent').click(function(){
        
        var textareavi = $(".clone .content_footer_en:last" ).find('textarea').attr('id');
        var textareaen = $(".clone .content_footer_vi:last" ).find('textarea').attr('id');
        // var textarea = $(".clone:last" ).find('textarea').attr('id');

        // var textarea = $(".clone:last" ).find('textarea').attr('id');
        tinymce.remove('#'.textareavi);
        tinymce.remove('#'.textareaen);
        $(".clone:last").clone().appendTo(".content_clone" );
        $('.content_clone .clone').find('.delete').on('click', function(){
            $(this).parent().remove();
        });
        // $(".clone:last").clone().appendTo(".content_footer_vi .content_clone" );
        // var id = Math.random();
        $(".clone .content_footer_en:last" ).find('textarea').attr('id',Math.random());
        $(".clone .content_footer_vi:last" ).find('textarea').attr('id',Math.random());
        $('.content_clone .clone').removeClass('hidden');
        tinyMCE();
        
       
    });
    $('.content_clone .clone').find('.delete').on('click', function(){
        $(this).parent().remove().show('slow');
    });
    // console.log($('.content_clone').find('.clone'));
   
    // $('#options').find('option').clone().appendTo('#options2');
    // $('.content_clone').find('.container-items').click(function(){
    //     console.log(this);
    //     // $(this).parent().remove();
    // }) ;
//======================================================================================
    $('.change-status').change(function(){

        if($(this).parent().hasClass('off')) {
            value = 0;
        }
        else {
            value = 1;
        }
        var url = $(this).attr('data-url');
        var id = $(this).attr('data-value');
        // console.log(value);
        changeStatus(id,value,url);
    }) ;
    function changeStatus(id,value,url) {
         $.ajax({
            url    : url,
            type   : 'GET',
            dataType: "text",
            data   : {id:id,value:value,url:url},
            success: function (data) 
            {   
                if(data) {
                    alert("Thay đổi trạng thái thành công");
                }
                else {
                    alert("Thay đổi trạng thái không thành công");
                }
            }
        })
    }   
    
    
//=======================================================================================
//Hiển thị hình ảnh của file image
    function readURL(input) {
        // alert("asda");
        // console.log($(input).parent().parent().find('img').attr('src'));
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $(input).parent().parent().find('img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#form-setting .logo,#form-slider .image,#form-slider-update .image,#form-add-product .image,.banner_post,.banner_visa").change(function(){
        readURL(this);
    });


//==========================================================================================
    
    $('#slider_backend').DataTable()
    // $('#slider_backend').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : true,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // });

    // if (($("#description").length > 0)){
    //      CKEDITOR.replace('description')
    // }
    // if (($("#content_footer_vi").length > 0)){
    //      CKEDITOR.replace('content_footer_vi');
    //      CKEDITOR.replace('content_footer_en');
    // }
//=====================================================================================
    $('.content_footer_en').slideToggle();
    $('.title_en').slideToggle();
    $('.change').click(function(){
        $('.content_footer_vi').slideToggle();
        $('.content_footer_en').slideToggle();
        $('.title_vi').slideToggle();
        $('.title_en').slideToggle();
    });
    // $('.change_title').click(function(){
        
    //     $('.title_vi').slideToggle();
    //     $('.title_en').slideToggle();
    // })
    // $("#clone").click(function(){
    //      $.ajax({
    //         type: "POST",
    //         url: '/product/render',
           
    //         success: function (data) {

    //             $('#need_clone').append(data);
    //             $("#need_clone .form-group input").each(function( index ) {
                   
    //                 $(this).attr('name',"tt" + parseInt(index + 1));
                    
                    
    //             });
              
    //         }
    //     });
    // })
// //================================= 
function tinyMCE(){
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

    tinymce.init({
        selector: "textarea",  // change this value according to your HTML
        // toolbar: "image,emoticons,checklist,link,numlist bullist",
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist  | forecolor backcolor   removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media  template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
        plugins: 'print preview fullpage   importcss  searchreplace autolink save directionality visualblocks visualchars fullscreen image link media  template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap  quickbars emoticons',
        // plugins: "image imagetools,emoticons,advlist,table,media,lists checklists,link,lists,pagebreak",
        images_upload_url: baseUrl+'/site/upload-file',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        images_upload_base_path:  baseUrl+'/../img/upload',
        images_reuse_filename: true,
        relative_urls: false,
        // height : "700",
        // and here's our custom image picker
        
    });  
}
tinyMCE();

    
    
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
// var origin   = window.location.origin; 
    // console.log(url.lastIndexOf('/'));
     var type= url.substr(url.lastIndexOf('/')+1);
    // $('.treeview-menu').click(function(){
    //     // $(this).addClass('active');
    //     console.log(this);
    // })
    $(".treeview-menu li").each(function() {
        // alert("sadas");
        // console.log(this);
        if($(this).attr('data-type') == type) {
            $(this).addClass('active');
            $(this).parent().parent().addClass('active');
        }
    })

});

