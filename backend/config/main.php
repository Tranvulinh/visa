<?php
// use \yii\web\Request;
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
// $baseUrl = str_replace('/backend/web', '/admin' , (new Request)->getBaseUrl());
// print_r($baseUrl);
// die();
return [
    // 'language' => 'vi',
    // 'sourceLanguage' => 'en-US',
    // 'on beforeAction' => require(__DIR__ . '/before-action.php'),
    // 'homeUrl' => '/administrator',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            // 'baseUrl' => $baseUrl,
            'class' => 'common\components\Request',
            'web'=> '/backend/web',
            'adminUrl' => '/admin'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                // 'username' => 'vulinh280694@gmail.com',
                // 'password' => 'gavhoearlvhgprpo',
                'username' => 'fptplayboxfshare@gmail.com',
                'password' => 'yceucupuialpjuxq',
                

                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        // 'homeUrl' => '/admin',
        'urlManager' => [
            // 'baseUrl' => $baseUrl,
             // 'baseUrl' => '',
            // 'baseUrl' => '/admin',
            // 'baseUrl' => $baseUrl.'/administrator',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // '' => 'site/index',  
                // '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            ],
            // 'scriptUrl'=>'/admin/index.php',
        ],
        // 'urlManagerFrontend' => [
        //     // 'enablePrettyUrl' => false,

        //     // 'class' => 'yii\web\UrlManager',

        //     // 'hostInfo' => 'http://localhost/frontend',

        //     'baseUrl' => $baseUrl,
        // ],
        
    ],
    'params' => $params,
];
