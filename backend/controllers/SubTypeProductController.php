<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\models\forms\SubTypeProductForm;
use common\models\SubTypeProduct;
use common\models\TypeProduct;
use common\models\Document;
use yii\helpers\ArrayHelper;
/**
 * TypeProduct controller
 */
class SubTypeProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-sub-type-product','update-sub-type-product','delete-sub-type-product','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = SubTypeProduct::getAllSubTypeProduct(); // lấy tất cả slider
        return $this->render('index',
            [
                'allData' => $allData,
            ]
        );
    }
    /**
     * render dữ liệu vào View [addSlider]
     */
    public function actionAddSubTypeProduct(){
        Yii::$app->view->title = "Thêm Visa";
        $model = new SubTypeProductForm();
        $allTypeProduct = TypeProduct::getAllTypeProduct(); 
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        $model->id = 0;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['SubTypeProductForm'];
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
            // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentEN'])) {
                $dataForm['contentEN'] = $dataForm['contentVI'] ;
            }
            // Kiểm tra nếu danh sách đại sứ quán tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['legationEN'])) {
                $dataForm['legationEN'] = $dataForm['legationVI'] ;
            }
            // $dataForm
            $dataForm['title'] = $dataForm['nameVI'];
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['legation'] = json_encode(array('vi' => $dataForm['legationVI'],'en' => $dataForm['legationEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            // print_r($dataForm);
            // die;
            $saveSetting = SubTypeProduct::updateSubTypeProduct($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                // remove element cuối cùng của mảng 
                array_pop($dataForm['dynamicInputVI']);
                array_pop($dataForm['dynamicInputEN']);
                array_pop($dataForm['dynamicContentVI']);
                array_pop($dataForm['dynamicContentEN']);
                /* Lưu thông tin document */
                $dataForm['sub'] = $saveSetting;
                if(count($dataForm['dynamicInputVI']) > 0) {
                    for ($i=0; $i < count($dataForm['dynamicInputVI'])   ; $i++) { 
                        $dataForm['titleDo'] = $dataForm['dynamicInputVI'][$i];
                        $dataForm['nameDo'] = json_encode(array('vi' => $dataForm['dynamicInputVI'][$i],'en' => $dataForm['dynamicInputEN'][$i])) ;
                        $dataForm['descriptionDo'] = json_encode(array('vi' => $dataForm['dynamicContentVI'][$i],'en' => $dataForm['dynamicContentEN'][$i]));
                        Document::updateDocument($dataForm);
                    }
                }
                // die();
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/sub-type-product']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/sub-type-product']);
            }  
        }       
        return $this->render('addSubTypeProduct',
            [
                'model' => $model,
                'map' => $map,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypeProduct]
     */
    public function actionUpdateSubTypeProduct(){
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new SubTypeProductForm();
        $data = SubTypeProduct::findOne($id); // Lấy dữ liệu từ db theo Id
        $allTypeProduct = TypeProduct::getAllTypeProduct(); 
        $allDocument = Document::getAllDocumentSub($id);
        // print_r($allDocument);
        // die;
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['SubTypeProductForm'];
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
                // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentEN'])) {
                    $dataForm['contentEN'] = $dataForm['contentVI'] ;
                }
                // Kiểm tra nếu danh sách đại sứ quán tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['legationEN'])) {
                    $dataForm['legationEN'] = $dataForm['legationVI'] ;
                }
                // $dataForm
                $dataForm['title'] = $dataForm['nameVI'];
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $dataForm['legation'] = json_encode(array('vi' => $dataForm['legationVI'],'en' => $dataForm['legationEN'])) ;
                $saveSetting = SubTypeProduct::updateSubTypeProduct($dataForm); // save form
                // print_r(Document::removeDocument($id));
                // die();
                // Document::removeDocument($saveSetting);
                if($saveSetting) { // nếu save setting thành công  
                    foreach ($allDocument as $key => $value) {
                        Document::removeDocument($value['id']);
                    }
                    // remove element cuối cùng của mảng 
                    array_pop($dataForm['dynamicInputVI']);
                    array_pop($dataForm['dynamicInputEN']);
                    array_pop($dataForm['dynamicContentVI']);
                    array_pop($dataForm['dynamicContentEN']);
                    /* Lưu thông tin document */
                    $dataForm['sub'] = $saveSetting;
                    // print_r($dataForm);
                    // die();
                    if(count($dataForm['dynamicInputVI']) > 0) {
                        for ($i=0; $i < count($dataForm['dynamicInputVI'])   ; $i++) { 
                            $dataForm['titleDo'] = $dataForm['dynamicInputVI'][$i];
                            $dataForm['nameDo'] = json_encode(array('vi' => $dataForm['dynamicInputVI'][$i],'en' => $dataForm['dynamicInputEN'][$i])) ;
                            $dataForm['descriptionDo'] = json_encode(array('vi' => $dataForm['dynamicContentVI'][$i],'en' => $dataForm['dynamicContentEN'][$i]));
                            $update = Document::updateDocument($dataForm);
                        }
                    }
                    Yii::$app->session->setFlash('success', "Update thành công");
                    // return $this->redirect(['/sub-type-product/add-sub-type-product']);
                    return $this->redirect(['/sub-type-product/update-sub-type-product','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    // return $this->redirect(['/sub-type-product/add-sub-type-product']);
                    return $this->redirect(['/sub-type-product/update-sub-type-product','id' => $id]);
                }  
            }    
            return $this->render('updateSubTypeProduct',
                [
                    'model' => $model,
                    'data' => $data,
                    'map' => $map,
                    'allDocument' => $allDocument,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            return $this->redirect(['/type-product']);
        }
    }
    public function actionDeleteSubTypeProduct() {
        $id = Yii::$app->request->get('id');
        $delete = SubTypeProduct::deleteSubTypeProduct($id);
        return $this->redirect(['/sub-type-product']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = SubTypeProduct::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = SubTypeProduct::changePosition($positions);
        return true;
    }
}
   