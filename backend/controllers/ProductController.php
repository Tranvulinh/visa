<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\models\forms\ProductForm;
use common\models\Product;
use common\models\CountryProduct;
use common\models\TypeProduct;
use common\models\SubTypeProduct;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
/**
 * Product controller
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        'actions' => [ 'index','add-product','update-product','delete-product','get-sub-type-product','get-country-product','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // [
                    //     'actions' => ['logout', 'index','upload-file','verify-email','change-url'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                ],
            ],
            
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = Product::getAllProduct(); // lấy tất cả slider
        return $this->render('index',
            [
                'allData' => $allData,
            ]
            
        );
    }

    /**
    * Lấy dữ liệu từ dropdownList TypeProduct hiển thị cho SubTypeProduct
    **/
    public function actionGetSubTypeProduct() {
        //use http://demos.krajee.com/widget-details/depdrop
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'][0];
            if ($id != null) {
                $out = SubTypeProduct::getSubTypeProduct($id); 
                foreach ($out as $key => $value) {
                    $out[$key]['name'] = json_decode($out[$key]['name'])->vi;
                }
                if(Yii::$app->request->get('id')) {
                    return ['output'=>$out, 'selected'=>Yii::$app->request->get('id')];

                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];


    }
    /**
    * Lấy dữ liệu từ dropdownList TypeProduct hiển thị cho SubTypeProduct
    **/
    public function actionGetCountryProduct() {
        //use http://demos.krajee.com/widget-details/depdrop
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'][0];
            if ($id != null) {
                $out = CountryProduct::getCountryProduct($id); 
                foreach ($out as $key => $value) {
                    $out[$key]['name'] = json_decode($out[$key]['name'])->vi;
                }
                if(Yii::$app->request->get('id')) {
                    return ['output'=>$out, 'selected'=>Yii::$app->request->get('id')];

                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];


    }
    public function actionAddProduct(){
        Yii::$app->view->title = "Thêm bài viết Visa";
        $model = new ProductForm();
        $allData = Product::getAllProduct(); // lấy tất cả slider
        $allTypeProduct = TypeProduct::getAllTypeProduct(); 
        // $allTSubypeProduct = SubTypeProduct::getAllSubTypeProduct(); 
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
           
        }
        $model->id = 0;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['ProductForm'];
            // print_r($dataForm);
            // die;

            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        

            }
            // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentEN'])) {
                $dataForm['contentEN'] = $dataForm['contentVI'] ;
            }
            // Kiểm tra nếu short_description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentshortEN'])) {
                $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
            }
            
            // $dataForm     
                       
            $dataForm['title'] = $dataForm['nameVI'];
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
            
            $saveSetting = Product::updateProduct($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/product']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/product']);
            }  
        }     
        return $this->render('addProduct',
            [
                'model' => $model,
                'map' => $map,
                // 'position' => $position,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypeProduct]
     */
    public function actionUpdateProduct(){
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        // $position = array(
        //     // '0' => 'Không hiển thị',
        //     '1' => 'Hiển thị menu',
        //     // '2' => 'Hiển thị trang chủ'
        // );
        $model = new ProductForm();

        $data = Product::findOne($id); // Lấy dữ liệu từ db theo Id
        $allTypeProduct = TypeProduct::getAllTypeProduct(); 
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
           
        }
        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['ProductForm'];
                
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        

                }
                // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentEN'])) {
                    $dataForm['contentEN'] = $dataForm['contentVI'] ;
                }
                // Kiểm tra nếu short_description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentshortEN'])) {
                    $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
                }
                // $dataForm
                $dataForm['title'] = $dataForm['nameVI'];
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
                
                $saveSetting = Product::updateProduct($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/product/update-product','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/product/update-product','id' => $id]);
                }  
            }    

            return $this->render('updateProduct',
                [
                    'model' => $model,
                    'data' => $data,
                    'map' => $map,
                    // 'position' => $position,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            Yii::$app->session->setFlash('error', "Không có dữ liệu");
                    
            return $this->redirect(['/product']);
        }
        
    }
    public function actionDeleteProduct() {
        $id = Yii::$app->request->get('id');
        $delete = Product::deleteProduct($id);
        return $this->redirect(['/product']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = Product::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = Product::changePosition($positions);
        return true;
    }
  
    
    // public function actionRender(){
    //     return $this->renderPartial('inputInfo');
    // }
    // public function actionAdd(){
    //     // $dataForm = Yii::$app->request->post();
    //     $dataForm = Yii::$app->request->post(); // lấy tất cả dữ liệu từ form trừ file
    //     $model = new ProductForm(); // tạo form kiểm tra đầu vào phía server
    //     $model->image = UploadedFile::getInstanceByName('image');
    //     if(!empty($dataForm['text_image'])){
    //         // nếu là update 
    //         $model->scenario = 'update';
    //     }
    //     else{
    //         // nếu là insert
    //         $model->scenario = 'insert';
    //     }
    //     // lấy thông tin hình ảnh 
    //     $model->image = UploadedFile::getInstanceByName('image'); 
    //     if($model->validate()){ // nếu validate thành công
    //         if($model->image) { //nếu có hình ảnh được upload
    //             $model->image->saveAs('img/product/' . $model->image->baseName . '.' . $model->image->extension); //save vào img/slider
    //             $image = 'img/product/'. $model->image->baseName . '.' . $model->image->extension;
    //             $dataForm = array_merge($dataForm,array('image' => $image));
    //         }
    //         $array1 = array();
    //         $array2 = array();
    //         foreach ($dataForm as $key => $value) {
    //             if(substr($key,0,2) == "tt"){
    //                 if(substr($key,-1) % 2 != 0){
    //                     $array1[] .= $value;
    //                     unset($dataForm[$key]);
    //                 }
    //                 else{
    //                     $array2[] .= $value;
    //                     unset($dataForm[$key]);

    //                 }  
    //             }
    //         }
    //         $dataForm['plus_info'] = array_combine($array1, $array2);
    //         $saveProduct = Product::updateProduct($dataForm); // thêm pr vào db
    //         if($saveProduct) { // nếu thêm slider thành công
    //             Yii::$app->session->setFlash('success', "Thêm thành công");
    //             return $this->redirect(['/product']);
    //         }  
    //         else { // nếu thêm slider không thành công
    //             Yii::$app->session->setFlash('error', "Thêm không thành công");
    //             return $this->redirect(['/product/add-product']);
    //         }  
    //     }
    //     else { // validate không thành công 
    //         $error = "";
    //         foreach ($model->errors as $key => $value) {
    //             $error .= $value[0] . "<br>";
    //         }
    //         Yii::$app->session->setFlash('error', $error);
    //         return $this->redirect(['/product/add-product']);
    //     }    
        
            
    // }
    // public function actionTest(){
    //     $model = new ProductForm();
    //     return $this->render('test',
    //         [
    //             'model' => $model,
    //         ]
    //     );
    // }
        
        
       
        
}
   
