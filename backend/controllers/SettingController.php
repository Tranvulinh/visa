<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\forms\SettingForm;
use common\models\Setting;
use yii\web\UploadedFile;

/**
 * Setting controller
 */
class SettingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex() {
        
        Yii::$app->view->title = "Backend setting";
        $title = 'Setting';
        $setting = Setting::getAllSetting();
       
        $model = new SettingForm();
        if (Yii::$app->request->isPost) {

            $model->logo = UploadedFile::getInstance($model, 'logo');
            $model->bannerPost = UploadedFile::getInstance($model, 'bannerPost');

            $model->bannerVisa = UploadedFile::getInstance($model, 'bannerVisa');
            $dataForm = Yii::$app->request->post()['SettingForm'];
            
            // print_r($dataForm);
            // die();
           
            if(! empty($model->logo->name)) { // nếu upload hình ảnh thành công
                $linklogo = $model->upload();
                $model->logo = null; 
                
                if($linklogo){
                    $dataForm['logo'] = $linklogo; // gán biến dataForm logo vào Form
                }              
            }   
            if(! empty($model->bannerPost->name)) { // nếu upload hình ảnh thành công
                $linkpost = $model->uploadBannerPost();
                $model->bannerPost = null; 
        // $model->save(false);
                if($linkpost){
                    $dataForm['bannerPost'] = $linkpost; // gán biến dataForm logo vào Form
                }              
            }  
           
            if(! empty($model->bannerVisa->name)) { // nếu upload hình ảnh thành công

                $linkvisa = $model->uploadBannerVisa();
                $model->bannerVisa = null; 

            //       print_r($dataForm);
            // die();
                if($linkvisa){
                    $dataForm['bannerVisa'] = $linkvisa; // gán biến dataForm logo vào Form
                }              
            }  


            $dataForm['contentFooter'] = json_encode(array('vi' => $dataForm['contentFooterVI'],'en' => $dataForm['contentFooterEN'])) ;
            $dataForm['contentContact'] = json_encode(array('vi' => $dataForm['contentContactVI'],'en' => $dataForm['contentContactEN'])) ;
            $saveSetting = Setting::updateSetting($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Update thành công");
                return $this->redirect(['/setting']);
            }
            else {
                Yii::$app->session->setFlash('error', "Update không thành công");
                return $this->redirect(['/setting']);
            }  
        }       
        return $this->render('index',
            [
                'setting' => $setting,
                'model' => $model,
                'title' => $title,
            ]
        );

    }
    public function pre($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
        die();
    }

    
}
