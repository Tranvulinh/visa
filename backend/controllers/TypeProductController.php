<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\models\forms\TypeProductForm;
use common\models\TypeProduct;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
/**
 * TypeProduct controller
 */
class TypeProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-type-product','update-type-product','delete-type-product','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = TypeProduct::getAllTypeProduct(); 
        return $this->render('index',
            [
                'allData' => $allData,
            ]
        );
    }
    /**
     * render dữ liệu vào View [addSlider]
     */
    public function actionAddTypeProduct(){
        Yii::$app->view->title = "Thêm Loại Visa";
        $model = new TypeProductForm();
        $model->id = 0;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['TypeProductForm'];
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
            // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentEN'])) {
                $dataForm['contentEN'] = $dataForm['contentVI'] ;
            }
            // $dataForm
            $dataForm['title'] = $dataForm['nameVI'];
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            $saveSetting = TypeProduct::updateTypeProduct($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/type-product']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/type-product/add-type-product']);
            }  
        }       
        return $this->render('addTypeProduct',
            [
                'model' => $model,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypeProduct]
     */
    public function actionUpdateTypeProduct(){
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new TypeProductForm();
        $data = TypeProduct::findOne($id); // Lấy dữ liệu từ db theo Id
        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['TypeProductForm'];
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
                // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentEN'])) {
                    $dataForm['contentEN'] = $dataForm['contentVI'] ;
                }
                // $dataForm                 
                $dataForm['title'] = $dataForm['nameVI'];
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $saveSetting = TypeProduct::updateTypeProduct($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    // return $this->redirect(['/type-product']);
                    return $this->redirect(['/type-product/update-type-product','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    // return $this->redirect(['/type-product']);
                    return $this->redirect(['/type-product/update-type-product','id' => $id]);
                }  
            }    
            return $this->render('updateTypeProduct',
                [
                    'model' => $model,
                    'data' => $data,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            return $this->redirect(['/type-product']);
        }
    }
    /* Delete Loại visa */ 
    public function actionDeleteTypeProduct() {
        $id = Yii::$app->request->get('id');
        $delete = TypeProduct::deleteTypeProduct($id);
        return $this->redirect(['/type-product']);
    }
    /* Thay đổi status loại visa */ 
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = TypeProduct::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    /* Thay đổi vị trí */ 
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = TypeProduct::changePosition($positions);
        return true;
    }
}
   