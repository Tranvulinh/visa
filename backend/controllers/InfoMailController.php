<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
// use common\models\forms\SettingForm;
use common\models\InfoMail;
// use yii\web\UploadedFile;

/**
 * Setting controller
 */
class InfoMailController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex() {
        
        Yii::$app->view->title = "Backend InfoMail";
        $title = 'Mail';
        $allData = InfoMail::getAllInfoMail();
       
        
        return $this->render('index',
            [
                'allData' => $allData,
                // 'model' => $model,
                // 'title' => $title,
            ]
        );

    }
    public function pre($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
        die();
    }

    
}
