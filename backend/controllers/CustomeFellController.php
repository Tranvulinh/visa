<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\forms\CustomeFellForm;
use common\models\CustomeFell;
use common\models\TypePost;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use yii\web\Response;
/**
 * Post controller
 */
class CustomeFellController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-custome-fell','update-custome-fell','delete-custome-fell','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = CustomeFell::getAllCustomeFell(); // lấy tất cả slider
        return $this->render('index',
            [
                'allData' => $allData,
            ]
        );
    }
    public function actionAddCustomeFell(){
        Yii::$app->view->title = "Thêm bài viết";
        $model = new CustomeFellForm();
        // $allData = Consultant::getAllConsultant(); 
        
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['CustomeFellForm'];
            // print_r($dataForm);
            // die();
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
           
           
            
            // $dataForm  
            // $dataForm['job'] = json_encode(array('vi' => $dataForm['jobVI'],'en' => $dataForm['jobEN'])) ;
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['company'] = json_encode(array('vi' => $dataForm['companyVI'],'en' => $dataForm['companyEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            
            $saveSetting = CustomeFell::updateCustomeFell($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/custome-fell']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/custome-fell']);
            }  
        }     
        // print_r($map);
        // die();
        return $this->render('addCustomeFell',
            [
                'model' => $model,
                // 'map' => $map,
                // 'position' => $position,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypePost]
     */
    public function actionUpdateCustomeFell(){
        Yii::$app->view->title = "Update bài viết";
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new CustomeFellForm();
        $data = CustomeFell::findOne($id); // Lấy dữ liệu từ db theo Id
        if($data){ // Nếu có dữ liệu
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['CustomeFellForm'];
                // print_r($dataForm);
                // die();
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
               
               
                
                // $dataForm  
                // $dataForm['job'] = json_encode(array('vi' => $dataForm['jobVI'],'en' => $dataForm['jobEN'])) ;
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['company'] = json_encode(array('vi' => $dataForm['companyVI'],'en' => $dataForm['companyEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $saveSetting = CustomeFell::updateCustomeFell($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/custome-fell/update-custome-fell','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/custome-fell/update-custome-fell','id' => $id]);
                }  
            }    
            return $this->render('updateCustomeFell',
                [
                    'model' => $model,
                    'data' => $data,
                    // 'map' => $map,
                    // 'position' => $position
                ]
            );
        }
        else { // Nếu không có dữ liệu
            Yii::$app->session->setFlash('error', "Không có dữ liệu");
            return $this->redirect(['/custome-fell']);
        }
    }
    public function actionDeleteCustomeFell() {
        $id = Yii::$app->request->get('id');
        $delete = CustomeFell::deleteCustomeFell($id);
        return $this->redirect(['/custome-fell']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = CustomeFell::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = CustomeFell::changePosition($positions);
        return true;
    }
}
   