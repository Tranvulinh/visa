<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\UploadedFile;
use common\models\forms\CountryProductForm;
use common\models\CountryProduct;
use common\models\SubTypeProduct;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
/**
 * Product controller
 */
class CountryProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-country-product','update-country-product','delete-country-product','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = CountryProduct::getAllCountryProduct(); // Lấy tất cả các nước
        return $this->render('index',
            [
                'allData' => $allData,
            ]
        );
    }
    /**
     * render dữ liệu vào View [Index]
     */
    // public function actionIndex(){
    //     $allData = Product::getAllProduct(); // lấy tất cả slider
    //     return $this->render('index',
    //         [
    //             'allData' => $allData,
    //         ]
    //     );
    // }
    /**
    * Lấy dữ liệu từ dropdownList TypeProduct hiển thị cho SubTypeProduct
    **/
    // public function actionGetSubTypeProduct() {
    //     //use http://demos.krajee.com/widget-details/depdrop
    //     Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //     $out = [];
    //     if (isset($_POST['depdrop_parents'])) {
    //         $id = $_POST['depdrop_parents'][0];
    //         if ($id != null) {
    //             $out = SubTypeProduct::getSubTypeProduct($id); 
    //             foreach ($out as $key => $value) {
    //                 $out[$key]['name'] = json_decode($out[$key]['name'])->vi;
    //             }
    //             if(Yii::$app->request->get('id')) {
    //                 return ['output'=>$out, 'selected'=>Yii::$app->request->get('id')];
    //             }
    //             return ['output'=>$out, 'selected'=>''];
    //         }
    //     }
    //     return ['output'=>'', 'selected'=>''];
    // }
    public function actionAddCountryProduct(){
        Yii::$app->view->title = "Thêm contry";
        $model = new CountryProductForm();
        $allTypeProduct = SubTypeProduct::getAllSubTypeProduct1(); // Lấy tất cả sub type product làm select (visa)
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        $model->id = 0;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['CountryProductForm'];
            // remove element cuối cùng của mảng 
            array_pop($dataForm['dynamicInputVI']);
            array_pop($dataForm['dynamicInputEN']);
            array_pop($dataForm['dynamicContentVI']);
            array_pop($dataForm['dynamicContentEN']);
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
            // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Nếu tab tiếng anh không có thì sẽ lấy tiếng việt
            $inputVI = $dataForm['dynamicInputVI'] ;
            $inputEN = $dataForm['dynamicInputEN'] ;
            foreach ($inputEN as $keyENI => $valueENI) {
                if($valueENI == "") {
                   $dataForm['dynamicInputEN'][$keyENI] = $inputVI[$keyENI];
                }
            }
            
            // $dataForm  lưu   
            $dataForm['title'] = $dataForm['nameVI'];  
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description_vi'] = json_encode(array('title' => $dataForm['dynamicInputVI'],'content' => $dataForm['dynamicContentVI'])) ;  
            $dataForm['description_en'] = json_encode(array('title' => $dataForm['dynamicInputEN'],'content' => $dataForm['dynamicContentEN'])) ;   
            $saveSetting = CountryProduct::updateCountryProduct($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/country-product']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/country-product']);
            }  
        }     
        return $this->render('addCountryProduct',
            [
                'model' => $model,
                'map' => $map,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypeProduct]
     */
    public function actionUpdateCountryProduct(){
        Yii::$app->view->title = "Update contry";
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        // print_r($id);
        // die();
        $model = new CountryProductForm();
        $data = CountryProduct::findOne($id); // Lấy dữ liệu từ db theo Id
        $allTypeProduct = SubTypeProduct::getAllSubTypeProduct1(); 
        $map = ArrayHelper::map($allTypeProduct,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['CountryProductForm'];
                // remove element cuối cùng của mảng 
                array_pop($dataForm['dynamicInputVI']);
                array_pop($dataForm['dynamicInputEN']);
                array_pop($dataForm['dynamicContentVI']);
                array_pop($dataForm['dynamicContentEN']);
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
                // Kiểm tra nếu name tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Nếu tab tiếng anh không có thì sẽ lấy tiếng việt
                $inputVI = $dataForm['dynamicInputVI'] ;
                $inputEN = $dataForm['dynamicInputEN'] ;
                foreach ($inputEN as $keyENI => $valueENI) {
                    if($valueENI == "") {
                       $dataForm['dynamicInputEN'][$keyENI] = $inputVI[$keyENI];
                    }
                }
                
                // $dataForm  lưu 
                $dataForm['title'] = $dataForm['nameVI'];    
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description_vi'] = json_encode(array('title' => $dataForm['dynamicInputVI'],'content' => $dataForm['dynamicContentVI'])) ;  
                $dataForm['description_en'] = json_encode(array('title' => $dataForm['dynamicInputEN'],'content' => $dataForm['dynamicContentEN'])) ;   
                $saveSetting = CountryProduct::updateCountryProduct($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/country-product/update-country-product','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/country-product/update-country-product','id' => $id]);
                }  
            }    
            return $this->render('updateCountryProduct',
                [
                    'model' => $model,
                    'data' => $data,
                    'map' => $map,
                    // 'position' => $position,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            Yii::$app->session->setFlash('error', "Không có dữ liệu");
            return $this->redirect(['/country-product']);
        }
    }
    public function actionDeleteCountryProduct() {
        $id = Yii::$app->request->get('id');
        $delete = CountryProduct::deleteCountryProduct($id);
        return $this->redirect(['/country-product']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = CountryProduct::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = CountryProduct::changePosition($positions);
        return true;
    }
}
   