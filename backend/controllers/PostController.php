<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\forms\PostForm;
use common\models\Post;
use common\models\TypePost;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use yii\web\Response;
/**
 * Post controller
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-post','update-post','delete-post','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = Post::getAllPost(); // lấy tất cả slider
        return $this->render('index',
            [
                'allData' => $allData,
            ]
        );
    }
    public function actionAddPost(){
        Yii::$app->view->title = "Thêm bài viết";
        $model = new PostForm();
        $allData = Post::getAllPost(); // lấy tất cả slider
        $allTypePost = TypePost::getAllTypePost(); 
        $map = ArrayHelper::map($allTypePost,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        $model->id = 0;
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['PostForm'];
            // print_r($dataForm);
            // die();
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentEN'])) {
                $dataForm['contentEN'] = $dataForm['contentVI'] ;
            }
            if(empty($dataForm['contentshortEN'])) {
                $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
            }
            // $dataForm
            $dataForm['title'] = $dataForm['nameVI'];    
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
            $saveSetting = Post::updatePost($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/post']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/post']);
            }  
        }     
        // print_r($map);
        // die();
        return $this->render('addPost',
            [
                'model' => $model,
                'map' => $map,
                // 'position' => $position,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypePost]
     */
    public function actionUpdatePost(){
        Yii::$app->view->title = "Update bài viết";
        $position = array(
            // '0' => 'Không hiển thị',
            '1' => 'Hiển thị menu',
            // '2' => 'Hiển thị trang chủ'
        );
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new PostForm();
        $data = Post::findOne($id); // Lấy dữ liệu từ db theo Id
        // echo "<pre>";
        // PRINT_R($data);
        // echo "</pre>";
        // die();
        $allTypePost = TypePost::getAllTypePost(); 
        $map = ArrayHelper::map($allTypePost,'id','name');
        foreach ($map as $key => $value) {
            $map[$key] = json_decode($value)->vi;
        }
        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['PostForm'];
                // print_r($dataForm);
                // die();
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentEN'])) {
                    $dataForm['contentEN'] = $dataForm['contentVI'] ;
                }
                if(empty($dataForm['contentshortEN'])) {
                    $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
                }
                // $dataForm
                // $dataForm['title_url'] = BaseInflector::slug(StringHelper::truncateWords($dataForm['nameVI'], 1000), "-") ;
                $dataForm['title'] = $dataForm['nameVI'];    
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
                $saveSetting = Post::updatePost($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/post/update-post','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/post/update-post','id' => $id]);
                }  
            }    
            return $this->render('updatePost',
                [
                    'model' => $model,
                    'data' => $data,
                    'map' => $map,
                    'position' => $position
                ]
            );
        }
        else { // Nếu không có dữ liệu
            Yii::$app->session->setFlash('error', "Không có dữ liệu");
            return $this->redirect(['/Post']);
        }
    }
    public function actionDeletePost() {
        $id = Yii::$app->request->get('id');
        $delete = Post::deletePost($id);
        return $this->redirect(['/post']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = Post::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = Post::changePosition($positions);
        return true;
    }
}
   