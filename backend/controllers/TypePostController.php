<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\forms\TypePostForm;
use common\models\TypePost;

/**
 * TypeProduct controller
 */
class TypePostController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        'actions' => [ 'index','add-type-post','update-type-post','delete-type-post','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        $allData = TypePost::getAllTypePost(); 
        return $this->render('index',
            [
                'allData' => $allData,
            ]
            
        );
    }
    /**
     * render dữ liệu vào View [addSlider]
     */

    public function actionAddTypePost(){
        Yii::$app->view->title = "Thêm loại bài viết";
       
        $model = new TypePostForm();
        $model->id = 0;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['TypePostForm'];
            
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        

            }
            if(empty($dataForm['nameEN'])) {
                $dataForm['nameEN'] = $dataForm['nameVI'];
            }
            // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentEN'])) {
                $dataForm['contentEN'] = $dataForm['contentVI'] ;
            }
            // Kiểm tra nếu short_description tiếng anh không có thì sẽ lấy tiếng việt
            if(empty($dataForm['contentshortEN'])) {
                $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
            }
            // $dataForm
            $dataForm['title'] = $dataForm['nameVI'];  
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
            
            $saveSetting = TypePost::updateTypePost($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/type-post']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/type-post']);
            }  
        }       
        return $this->render('addTypePost',
            [
                'model' => $model,
                // 'position' => $position,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateTypePost]
     */
    public function actionUpdateTypePost(){
        Yii::$app->view->title = "Update loại bài viết";

        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new TypePostForm();
        $data = TypePost::findOne($id); // Lấy dữ liệu từ db theo Id

        if($data){ // Nếu có dữ liệu
            $model->id = $id;
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['TypePostForm'];
                
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        

                }
                if(empty($dataForm['nameEN'])) {
                    $dataForm['nameEN'] = $dataForm['nameVI'];
                }
                // Kiểm tra nếu description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentEN'])) {
                    $dataForm['contentEN'] = $dataForm['contentVI'] ;
                }
                // Kiểm tra nếu short_description tiếng anh không có thì sẽ lấy tiếng việt
                if(empty($dataForm['contentshortEN'])) {
                    $dataForm['contentshortEN'] = $dataForm['contentshortVI'];
                }
                // $dataForm
                $dataForm['title'] = $dataForm['nameVI'];  
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $dataForm['short_description'] = json_encode(array('vi' => $dataForm['contentshortVI'],'en' => $dataForm['contentshortEN'])) ;
                
                $saveSetting = TypePost::updateTypePost($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/type-post/update-type-post' ,'id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/type-post/update-type-post','id' => $id]);
                }  
            }    
            return $this->render('updateTypePost',
                [
                    'model' => $model,
                    'data' => $data,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            return $this->redirect(['/type-post']);
        }
        
    }
    public function actionDeleteTypePost() {
        $id = Yii::$app->request->get('id');
        $delete = TypePost::deleteTypePost($id);
        return $this->redirect(['/type-post']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = TypePost::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = TypePost::changePosition($positions);
        return true;
    }
   
}
   
