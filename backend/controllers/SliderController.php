<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Slider;
use yii\web\UploadedFile;
use common\models\forms\SliderForm;
/**
 * Slider controller
 */
class SliderController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','add-slider','update-slider','delete-slider','change-status','change-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false; // use method post
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * render dữ liệu vào View [Index]
     */
    public function actionIndex(){
        Yii::$app->view->title = "Danh sách Slider";
        $allSlider = Slider::getAllSlider(); // lấy tất cả slider
        return $this->render('index',
            [
                'slider' => $allSlider,
            ]
        );
    }
    /**
     * render dữ liệu vào View [addSlider]
     */
    public function actionAddSlider(){
        Yii::$app->view->title = "Thêm Slider";
        $model = new SliderForm();
        if (Yii::$app->request->isPost) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $dataForm = Yii::$app->request->post()['SliderForm'];
            if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                $link = $model->upload(); // lưu vào path
                if($link){ // nếu có link trả về
                    $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                }        
            }
            // $dataForm
            $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
            $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
            $saveSetting = Slider::updateSlider($dataForm); // save form
            if($saveSetting) { // nếu save setting thành công  
                Yii::$app->session->setFlash('success', "Thêm thành công");
                return $this->redirect(['/slider']);
            }
            else {
                Yii::$app->session->setFlash('error', "Thêm không thành công");
                return $this->redirect(['/slider']);
            }  
        }     
        return $this->render('addSlider',
            [
                'model' => $model,
            ]
        );
    }
    /**
     * render dữ liệu vào View [updateSlider]
     */
    public function actionUpdateSlider(){
        Yii::$app->view->title = "Update Slider";
        $id = Yii::$app->request->get('id'); // lấy dữ liệu từ URL
        $model = new SliderForm();
        $data = Slider::findOne($id); // Lấy dữ liệu từ db theo Id
        if($data){ // Nếu có dữ liệu
            if (Yii::$app->request->isPost) {
                $model->image = UploadedFile::getInstance($model, 'image');
                $dataForm = Yii::$app->request->post()['SliderForm'];
                if(! empty($model->image->name)) { // nếu upload hình ảnh thành công
                    $link = $model->upload(); // lưu vào path
                    if($link){ // nếu có link trả về
                        $dataForm['image'] = $link; // gán biến dataForm logo vào Form
                    }        
                }
                // $dataForm
                $dataForm['name'] = json_encode(array('vi' => $dataForm['nameVI'],'en' => $dataForm['nameEN'])) ;
                $dataForm['description'] = json_encode(array('vi' => $dataForm['contentVI'],'en' => $dataForm['contentEN'])) ;
                $saveSetting = Slider::updateSlider($dataForm); // save form
                if($saveSetting) { // nếu save setting thành công  
                    Yii::$app->session->setFlash('success', "Update thành công");
                    return $this->redirect(['/slider/update-slider','id' => $id]);
                }
                else {
                    Yii::$app->session->setFlash('error', "Update không thành công");
                    return $this->redirect(['/slider/update-slider','id' => $id]);
                }  
            }    
            return $this->render('updateSlider',
                [
                    'model' => $model,
                    'data' => $data,
                ]
            );
        }
        else { // Nếu không có dữ liệu
            Yii::$app->session->setFlash('error', "Không có dữ liệu");
            return $this->redirect(['/slider']);
        }
    }
    public function actionDeleteSlider(){
        $id = Yii::$app->request->get('id');
        $delete = Slider::deleteSlider($id);
        return $this->redirect(['/slider']);
    }
    public function actionChangeStatus() {
        $id = Yii::$app->request->get('id');
        $value = Yii::$app->request->get('value');
        $change = Slider::changeStatus($id,$value);
        if($change) {
            return true;
        } 
        else {
            return false;
        }
        // return $this->redirect(['/product']);
    }
    public function actionChangePosition() {
        $positions = Yii::$app->request->get('positions');
        $change = Slider::changePosition($positions);
        return true;
    }
}