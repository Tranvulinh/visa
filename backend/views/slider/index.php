<?php
use yii\helpers\Url;
?>
<section class="content">
  <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
               <a class="btn btn-success" href="<?php echo Url::to(['/slider/add-slider']);  ?>">
                  <i class="fa fa-plus"></i> Thêm mới
               </a>
            </div>
            <div class="box-body">
                <table id="slider_backend" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                          <th>STT</th>
                          <th>Image</th>
                          <th>Status</th>                          
                          <th>Sửa</th>                          
                          <th>Xóa</th>                          
                        </tr>
                    </thead>
                    <tbody id="sortable">
                        <?php
                        $stt = 1; 
                        foreach ($slider as $sl) : ?>
                           <tr class="ui-state-default ui-sortable-handle" data-position="<?php echo $sl['positions'] ?> " data-id="<?php echo $sl['id'] ?>" data-url="slider/change-position">
                              <input type="hidden" name="id" value="<?php echo $sl['id'] ?>">
                              <td><?php echo $stt ++ ?></td>
                              <td style="width:20%">
                                 <?php if($sl['image'] == "") : ?>
                                    <div></div>
                                 <?php else : ?>   
                                    <img src="<?php echo Url::to('../'.$sl['image']); ?>" width=100px height=100px>
                                 <?php endif ?>
                              </td>
                              <td>
                             <?php if($sl['status'] == 1): ?>
                                 <input data-url="slider/change-status" data-value="<?php echo $sl['id']  ?>" name="status" class="status change-status" type="checkbox" checked data-toggle="toggle" data-size="sm" >
                              <?php else : ?>
                                 <input data-url="slider/change-status" data-value="<?php echo $sl['id'] ?>" name="status" class="status change-status" type="checkbox" data-toggle="toggle" data-size="sm">
                              <?php endif ?>
                              </td>
                              <td class="text-center">
                                <a href="<?php echo Url::to(['/slider/update-slider', 'id' => $sl['id']]);  ?>"><i class="glyphicon glyphicon-edit"></i></a>
                              </td>
                              <td class="text-center">
                                <a href="<?php echo Url::to(['/slider/delete-slider', 'id' => $sl['id']]);  ?>"> <i class="fa fa-remove"></i></a>
                              </td>
                           </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</section>