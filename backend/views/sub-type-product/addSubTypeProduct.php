<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use conquer\select2\Select2Widget;
?>
<div class="box box-primary">
	<?php  echo Yii::$app->controller->renderPartial('/layouts/breadcum')  ?>
	<div class="row">
		<div class="col-md-12">
		 	<?php if (Yii::$app->session->hasFlash('success')): ?>
			    <div class="alert alert-success alert-dismissable"> 
			        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
			    </div>
			<?php endif; ?> 
			<?php if (Yii::$app->session->hasFlash('error')): ?>
			    <div class="alert alert-error alert-dismissable"> 
			        <?= Yii::$app->session->getFlash('error') ?>
			    </div>
			<?php endif; ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    		<?php $form = ActiveForm::begin(
			[
				'id' => 'form-add-product',
				'enableAjaxValidation' => true,
				'options' => [
			        'enctype' => 'multipart/form-data'
			    ]
			]);?>
			<div class="box-body">
		  		<div class="form-group row">
		  			<label for="name" class="col-sm-2 col-form-label">
						<div>Tên (*)</div>
						<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
					</label>
					<div class="col-sm-10">
							<div class="title_vi">
								<?= $form->field($model, 'nameVI')->textInput(['autofocus' => true])->label('Ngôn ngữ tiếng việt') ?>
							</div>
							<div class="title_en">
								<?= $form->field($model, 'nameEN')->textInput(['autofocus' => true])->label('Ngôn ngữ tiếng anh') ?>
							</div>
						</div>
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Mô tả </div>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi'
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en'
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Danh sách đại sứ quán</div>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'legationVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'legationVI'
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'legationEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'legationEN'
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
		  		</div>
		  		<div class="form-group row">
		  			<label for="name" class="col-sm-2 col-form-label">
						<div><button style="width: 100%" type="button" class="add-item btn btn-success plusContent"><i class="fa fa-plus"></i> Thêm mới</button></div>
					</label>
					<div class="col-sm-10 align-self-end ">
						<div class="panel-heading">
				            <i class="fa fa-envelope"></i> Tài liệu
				            <div class="clearfix"></div>
				        </div>
				        <div class="content_clone">
						</div>
						<div class="panel-body container-items clone hidden">
						    <button type="button" class="delete btn">X</button>
				        	<div class="content_footer_vi">
								<?= $form->field($model, 'dynamicInputVI[]')->textInput(['autofocus' => true,'class' => 'form-control'])->label('Content') ?>
								<?= $form->field($model, 'dynamicContentVI[]')->textarea([
							  		'rows' => '12',
							  		'class' => 'text',
							  		'id' => 'dynamicContentVI',
							  	])->label(false) ?>
							</div>
							<div class="content_footer_en">
								<?= $form->field($model, 'dynamicInputEN[]')->textInput(['autofocus' => true,'class' => 'form-control'])->label('Content') ?>
								<?= $form->field($model, 'dynamicContentEN[]')->textarea([
							  		'rows' => '12',
							  		'class' => 'text',
							  		'id' => 'dynamicContentEN',
							  	])->label(false) ?>
							</div>
				        </div>
					</div>
		  		</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Loại Visa
					</label>
					<div class="col-sm-8">
				        <?= $form->field($model, 'typeProduct')->widget(
					    Select2Widget::className(),
					    [
					        'items'=> $map
					    ]
						)->label(false); ?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Hình Ảnh
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'image')->fileInput(['class' => 'image', 'accept'=>".png,.jpg" ])->label(false) ?>
					  	<div class="display_image">
					  		<img src="" width=200px>
					  	</div>
					</div>
				</div>
				<div class="form-group row">
					<label  class="col-sm-2 control-label">
						Status
					</label>
					<div class="col-sm-10">
					  	<?= $form
                        ->field($model, 'status')
                        ->checkbox([
                        	"data-toggle" => "toggle", 
                        	"data-size"=>"sm",
                        	"checked"=>"checked"
                        ])->label(false)?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<div class="col-sm-2"></div>
			  		<div class="col-sm-2">
					 	<button type="submit" class="btn btn-info ">Cập Nhật</button>
					</div>
			  	</div>
	 		</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>