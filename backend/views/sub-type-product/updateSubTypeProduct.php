<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use conquer\select2\Select2Widget;
?>
<div class="box box-primary">
	<?php  echo Yii::$app->controller->renderPartial('/layouts/breadcum')  ?>
	<div class="row">
		<div class="col-md-12">
		 	<?php if (Yii::$app->session->hasFlash('success')): ?>
			    <div class="alert alert-success alert-dismissable"> 
			        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
			    </div>
			<?php endif; ?> 
			<?php if (Yii::$app->session->hasFlash('error')): ?>
			    <div class="alert alert-error alert-dismissable"> 
			        <?= Yii::$app->session->getFlash('error') ?>
			    </div>
			<?php endif; ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    		<?php $form = ActiveForm::begin(
			[
				'enableAjaxValidation' => true,
				'id' => 'form-add-product',
				'options' => [
			        'enctype' => 'multipart/form-data'
			    ]
			]);?>
			<div class="box-body">
		  		<div class="form-group row">
		  			<?= $form->field($model, 'id')->hiddenInput(['value'=>$data['id']])->label(false); ?>
		  			<label for="name" class="col-sm-2 col-form-label">
						<div>Tên (*)</div>
						<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
					</label>
					<div class="col-sm-10">
							<div class="title_vi">
								<?= $form->field($model, 'nameVI')->textInput([
									'autofocus' => true,
									'value' =>  !empty(json_decode($data['name'])->vi) ? json_decode($data['name'])->vi : " "
								])->label('Ngôn ngữ tiếng việt') ?>
							</div>
							<div class="title_en">
								<?= $form->field($model, 'nameEN')->textInput([
									'autofocus' => true,
									'value' => !empty(json_decode($data['name'])->en) ? json_decode($data['name'])->en : " "
								])->label('Ngôn ngữ tiếng anh') ?>
							</div>
						</div>
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Mô tả </div>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi',
						  		'value' => !empty(json_decode($data['description'])->vi) ? json_decode($data['description'])->vi : " "
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en',
						  		'value' => !empty(json_decode($data['description'])->en) ? json_decode($data['description'])->en : " "
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Danh sách đại sứ quán</div>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'legationVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi',
						  		'value' => !empty(json_decode($data['legation'])->vi) ? json_decode($data['legation'])->vi : " "
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'legationEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en',
						  		'value' => !empty(json_decode($data['legation'])->en) ? json_decode($data['legation'])->en : " "
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
		  			<label for="name" class="col-sm-2 col-form-label">
						<div><button style="width: 100%" type="button" class="add-item btn btn-success plusContent"><i class="fa fa-plus"></i> Thêm mới</button></div>
					</label>
					<div class="col-sm-10 align-self-end ">
						<div class="panel-heading">
				            <i class="fa fa-envelope"></i> Tài liệu
				            <div class="clearfix"></div>
				        </div>
				        <div class="content_clone">
				        	
				        		
				        		
			        		<?php for ($i=0; $i < count($allDocument); $i++) { ?>
		        			<div class="panel-body container-items clone">
			        			<button type="button" class="delete btn">X</button>
			        			<div class="content_footer_vi">
				                    <?= $form->field($model, 'dynamicInputVI[]')->textInput([
				        				'value' => json_decode($allDocument[$i]['name'])->vi,
				        				'class' => 'form-control'
				        			])->label('Content') ;?>
				        			<?= $form->field($model, 'dynamicContentVI[]')->textarea([
								  		'rows' => '12',
								  		'class' => 'text',
								  		'id' => 'dynamicContentVI'.$i,
								  		'value' => json_decode($allDocument[$i]['description'])->vi
								  	])->label(false); ?>
							  	</div>
							  	<div class="content_footer_en">
								  	<?= $form->field($model, 'dynamicInputEN[]')->textInput([
			        				'value' => json_decode($allDocument[$i]['name'])->en,
			        				'class' => 'form-control'
				        			])->label('Content'); ?>
				        			<?= $form->field($model, 'dynamicContentEN[]')->textarea([
								  		'rows' => '12',
								  		'class' => 'text',
								  		'id' => 'dynamicContentEN'.$i,
								  		'value' => json_decode($allDocument[$i]['description'])->en
								  	])->label(false);?>
							  	</div>
			                
			            	</div>
			                <?php } ?>					        	
								
								
									
					                    
							  
					        </div>
						</div>
						<div class="panel-body container-items clone hidden">
							<button type="button" class="delete btn">X</button>
				        	<div class="content_footer_vi">
								<?= $form->field($model, 'dynamicInputVI[]')->textInput(['autofocus' => true,'class' => 'form-control'])->label('Content Tab') ?>
								<?= $form->field($model, 'dynamicContentVI[]')->textarea([
							  		'rows' => '12',
							  		'class' => 'text',
							  		'id' => 'dynamicContentVIi',
							  	])->label(false) ?>
							</div>
							<div class="content_footer_en">
								<?= $form->field($model, 'dynamicInputEN[]')->textInput(['autofocus' => true,'class' => 'form-control'])->label('Content Tab') ?>
								<?= $form->field($model, 'dynamicContentEN[]')->textarea([
							  		'rows' => '12',
							  		'class' => 'text',
							  		'id' => 'dynamicContentENi',
							  	])->label(false) ?>
							</div>
				        </div>
					</div>
		  		</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Loại Visa
					</label>
					<div class="col-sm-8">
				        <?= $form->field($model, 'typeProduct')->widget(
						    Select2Widget::className(),
						    [
						        'items'=> $map,
						        // 'name' => 'Kani',
						        // 'value' => $data['id_sub_type_product'],
						        'options' => [ 
						        	'value' => $data['id_type_product'],
						        	// 'minimumInputLength' => 2,
						        	// 'multiple' => true,
						    	]
						    ]
						)->label(false); ?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Hình Ảnh
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'image')->fileInput([
  							'class' => 'image',
  							'accept'=>".png,.jpg",
  							'value' => Url::to([$data['image']])
  						])->label(false) ?>
					  	<div class="display_image">
					  		<?php if($data['image'] == "") : ?>
					  			<img src="" width=200px>
					  		<?php else : ?>
					  			<img src="<?php echo Url::to('../../'.$data['image'])  ?>" width=200px>
					  		<?php endif ?>
					  	</div>
					</div>
				</div>
				<div class="form-group row">
					<label  class="col-sm-2 control-label">
						Status
					</label>
					<div class="col-sm-10">
						<?php ($data['status'] == 1) ? $check = true : $check = false ?>
					  	<?= $form
                        ->field($model, 'status')
                        ->checkbox([
                        	"data-toggle" => "toggle", 
                        	"data-size"=>"sm",
                        	"checked"=>$check,
                        ])->label(false)?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<div class="col-sm-2"></div>
			  		<div class="col-sm-2">
					 	<button type="submit" class="btn btn-info ">Cập Nhật</button>
					</div>
			  	</div>
	 		</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>