<?php
use yii\helpers\Url;
?>
<style>
  ul{padding-left:0px;}
  .padding{
    padding:10px;
  }
</style>
<section class="content">
  <div class="row">
      <div class="col-xs-12">
      

        <div class="box">
            
            <div class="box-body">
                <table id="slider_backend" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                          <th>STT</th>
                          <th>Tên</th>
                          <!-- <th>Description</th> -->
                          <th>Email</th>
                          <th>Số Điện Thoại</th>                          
                          <th>Địa Chỉ</th>                          
                          <th>Nội Dung</th>                          
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 1; 
                        foreach ($allData as $ad) : ?>
                           <tr>
                              <input type="hidden" name="id" value="<?php echo $ad['id'] ?>">
                              <td><?php echo $stt ++ ?></td>
                              <td style="text-align: left;padding:0px">
                                <!-- <ul> -->
                                  <!-- <div class="label bg-green">Tiếng Việt</div> -->
                                  <div class="padding"><?php echo  $ad['name']?></div>
                                  
                                
                               
                              </td>
                              <td style="text-align: left;padding:0px">
                                <!-- <ul> -->
                                  <!-- <div class="label bg-green">Tiếng Việt</div> -->
                                  <div class="padding"><?php echo  $ad['email']?></div>
                                  
                                
                               
                              </td>
                              <td style="text-align: left;padding:0px">
                                <!-- <ul> -->
                                  <!-- <div class="label bg-green">Tiếng Việt</div> -->
                                  <div class="padding"><?php echo  $ad['phone']?></div>
                                  
                                
                               
                              </td>
                              <td style="text-align: left;padding:0px">
                                <!-- <ul> -->
                                  <!-- <div class="label bg-green">Tiếng Việt</div> -->
                                  <div class="padding"><?php echo  $ad['address']?></div>
                                  
                                
                               
                              </td>
                              <td style="text-align: left;padding:0px">
                                <!-- <ul> -->
                                  <!-- <div class="label bg-green">Tiếng Việt</div> -->
                                  <div class="padding"><?php echo  $ad['message']?></div>
                                  
                                
                               
                              </td>
                              
                             
                             
                           </tr>
                        <?php endforeach ?>
                    </tbody>
                
                </table>
            </div>
        
        </div>

    </div>
    
  </div>
 
</section>