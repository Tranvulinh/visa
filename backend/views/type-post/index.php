<?php
use yii\helpers\Url;
?>
<style>
  ul{padding-left:0px;}
  .padding{
    padding:10px;
  }
</style>
<section class="content">
  <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
               <a class="btn btn-success" href="<?php echo Url::to(['/type-post/add-type-post']);  ?>">
                  <i class="fa fa-plus"></i> Thêm mới
               </a>
            </div>
            <div class="box-body">
                <table id="slider_backend" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                          <th>STT</th>
                          <th>Tên</th>
                          <!-- <th>Description</th> -->
                          <th>Image</th>
                          <th>Status</th>                          
                          <th>Sửa</th>                          
                          <th>Xóa</th>                          
                        </tr>
                    </thead>
                    <tbody id="sortable">
                        <?php
                        $stt = 1; 
                        foreach ($allData as $ad) : ?>
                           <tr class="ui-state-default ui-sortable-handle" data-position="<?php echo $ad['positions'] ?> " data-id="<?php echo $ad['id'] ?>" data-url="type-post/change-position">
                              <input type="hidden" name="id" value="<?php echo $ad['id'] ?>">
                              <td><?php echo $stt ++ ?></td>
                              <td style="text-align: left;padding:0px">
                                
                                  <div class="padding"><?php echo !empty(json_decode($ad['name'])->vi) ? json_decode($ad['name'])->vi : "" ?></div>
                                 
                              </td>
                             
                              <td style="width:20%">
                                 <?php if($ad['image'] == "") : ?>
                                    <div></div>
                                 <?php else : ?>   
                                    <img src="<?php echo Url::to('../'.$ad['image']); ?>" width=100px height=100px>
                                 <?php endif ?>
                              </td>
                              <td>
                              <?php if($ad['status'] == 1): ?>
                                 <input data-url="type-post/change-status" data-value="<?php echo $ad['id']  ?>" name="status" class="status change-status" type="checkbox" checked data-toggle="toggle" data-size="sm" >
                              <?php else : ?>
                                 <input data-url="type-post/change-status" data-value="<?php echo $ad['id'] ?>" name="status" class="status change-status" type="checkbox" data-toggle="toggle" data-size="sm">
                              <?php endif ?>
                              </td>
                              <td class="text-center">
                                <a href="<?php echo Url::to(['/type-post/update-type-post', 'id' => $ad['id']]);  ?>"><i class="glyphicon glyphicon-edit"></i></a>
                              </td>
                              <td class="text-center">
                                <a href="<?php echo Url::to(['/type-post/delete-type-post', 'id' => $ad['id']]);  ?>"> <i class="fa fa-remove"></i></a>
                              </td>
                           </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</section>