<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\depdrop\DepDrop;
?>
<style>
	.has-error .help-block-error{
		transition:height 0.3s ease-out; 
		height: 15px;
		font-style: italic;
    	font-weight: bold;
	}
	.help-block-error{		 
		 height:0px;
		 transition:height 0.3s ease-out;
	}
	input {
		border:1px solid !important;
	}
	label {
    	font-weight: 400;
    	color: black;
	}
	.form-control:focus{
		border:2px solid !important;
		border-color: #3c8dbc !important;
    	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, 0.6);
	}
	.box-body{
		padding:25px;
	}
	.hidden{
		transition: 0.5s;
	}
	.show{
		transition: 0.5s;
	}
</style>
<div class="box box-primary">
	<?php  echo Yii::$app->controller->renderPartial('/layouts/breadcum')  ?>
	<div class="row">
		<div class="col-md-12">
		 	<?php if (Yii::$app->session->hasFlash('success')): ?>
			    <div class="alert alert-success alert-dismissable"> 
			        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
			    </div>
			<?php endif; ?> 
			<?php if (Yii::$app->session->hasFlash('error')): ?>
			    <div class="alert alert-error alert-dismissable"> 
			        <?= Yii::$app->session->getFlash('error') ?>
			    </div>
			<?php endif; ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    		<?php $form = ActiveForm::begin(
			[
				'enableAjaxValidation' => true,
				'id' => 'form-add-product',
				'options' => [
			        'enctype' => 'multipart/form-data'
			    ]
			]);?>
			<div class="box-body">
		  		<div class="form-group row">
		  			<label for="name" class="col-sm-2 col-form-label">
						<div>Tên (*)</div>
						<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
					</label>
					<div class="col-sm-8">
						<div class="title_vi">
							<?= $form->field($model, 'nameVI')->textInput(['autofocus' => true,'class' => 'form-control nameVi'])->label('Ngôn ngữ tiếng việt') ?>
						</div>
						<?= $form->field($model, 'titleURL')->hiddenInput([
                	
		                    'type'  => 'hidden',
		                    'class' => 'titleUrl',
		               

		                ])->label(false)?>
						<div class="title_en">
							<?= $form->field($model, 'nameEN')->textInput(['autofocus' => true])->label('Ngôn ngữ tiếng anh') ?>
						</div>
					</div>
		  		</div>
		  		
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Mô tả </div>
						</label>
						<div class="col-sm-8">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi'
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en'
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Mô tả ngắn</div>
						</label>
						<div class="col-sm-8">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentshortVI')->textInput([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi'
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentshortEN')->textInput([
						  		'rows' => '12',
						  		'id' => 'content_footer_en'
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Loại Visa
					</label>
					<div class="col-sm-8">
  						<?= $form->field($model, 'typeProduct')
					        ->dropDownList(
					            $map,
					            ['prompt'=>'Chọn loại Visa']
					        )->label(false); ?>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Visa
					</label>
					<div class="col-sm-8">
  						<?= $form->field($model, 'subTypeProduct')->widget(DepDrop::classname(), [
							     'options' => ['id'=>'subtypeproduct'],
							     'pluginOptions'=>[
							         'depends'=>['productform-typeproduct'],
							         'placeholder' => 'Select...',
							         'url' => Url::to(['product/get-sub-type-product'])
							     ]
							 ])->label(false);?>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Country
					</label>
					<div class="col-sm-8">
  						<?= $form->field($model, 'countryProduct')->widget(DepDrop::classname(), [
							     'options' => ['id'=>'coutryproduct'],
							     'pluginOptions'=>[
							         'depends'=>['subtypeproduct'],
							         'placeholder' => 'Select...',
							         'url' => Url::to(['product/get-country-product'])
							     ]
							 ])->label(false);?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Hình Ảnh
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'image')->fileInput(['class' => 'image', 'accept'=>".png,.jpg" ])->label(false) ?>
					  	<div class="display_image">
					  		<img src="" width=200px>
					  	</div>
					  	<div>Hình Ảnh (370x240)</div>
					</div>
				</div>
				<div class="form-group row">
					<label  class="col-sm-2 control-label">
						Status
					</label>
					<div class="col-sm-10">
					  	<?= $form
                        ->field($model, 'status')
                        ->checkbox([
                        	"data-toggle" => "toggle", 
                        	"data-size"=>"sm",
                        	"checked"=>"checked"
                        ])->label(false)?>
					</div>
				</div>

		  		<div class="form-group row">
		  			<div class="col-sm-2"></div>
			  		<div class="col-sm-2">
					 	<button type="submit" class="btn btn-info "><i class="fa fa-plus"></i> Thêm</button>
					 	
					</div>
			  	</div>
	 		</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
