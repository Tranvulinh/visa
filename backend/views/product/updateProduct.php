<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\depdrop\DepDrop;
?>

<div class="box box-primary">
	<?php  echo Yii::$app->controller->renderPartial('/layouts/breadcum')  ?>
	<div class="row">
		<div class="col-md-12">
		 	<?php if (Yii::$app->session->hasFlash('success')): ?>
			    <div class="alert alert-success alert-dismissable"> 
			        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
			    </div>
			<?php endif; ?> 
			<?php if (Yii::$app->session->hasFlash('error')): ?>
			    <div class="alert alert-error alert-dismissable"> 
			        <?= Yii::$app->session->getFlash('error') ?>
			    </div>
			<?php endif; ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    		<?php $form = ActiveForm::begin(
			[
				'enableAjaxValidation' => true,
				'id' => 'form-add-product',
				'options' => [
			        'enctype' => 'multipart/form-data'
			    ]
			]);?>
			<div class="box-body">
		  		<div class="form-group row">
		  			<?= $form->field($model, 'id')->hiddenInput(['value'=>$data['id']])->label(false); ?>
		  			<label for="name" class="col-sm-2 col-form-label">
						<div>Tên (*)</div>
						<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
					</label>
					<div class="col-sm-8">
							<div class="title_vi">
								<?= $form->field($model, 'nameVI')->textInput([
									'autofocus' => true,
									'value' =>  !empty(json_decode($data['name'])->vi) ? json_decode($data['name'])->vi : " "
								])->label('Ngôn ngữ tiếng việt') ?>
							</div>
							<?= $form->field($model, 'titleURL')->hiddenInput([
                	
			                    'type'  => 'hidden',
			                    'class' => 'titleUrl',
			               

			                ])->label(false)?>
							<div class="title_en">
								<?= $form->field($model, 'nameEN')->textInput([
									'autofocus' => true,
									'value' => !empty(json_decode($data['name'])->en) ? json_decode($data['name'])->en : " "
								])->label('Ngôn ngữ tiếng anh') ?>
							</div>
						</div>
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Mô tả </div>
						</label>
						<div class="col-sm-8">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi',
						  		'value' => !empty(json_decode($data['description'])->vi) ? json_decode($data['description'])->vi : " "
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en',
						  		'value' => !empty(json_decode($data['description'])->en) ? json_decode($data['description'])->en : " "
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						<div>Mô tả ngắn</div>
					</label>
					<div class="col-sm-8">
						<div class="content_footer_vi">
					  	<?= $form->field($model, 'contentshortVI')->textInput([
					  		'rows' => '12',
					  		'id' => 'content_footer_vi',
					  		'value' => !empty(json_decode($data['short_description'])->vi) ? json_decode($data['short_description'])->vi : " "
					  	])->label('Ngôn ngữ tiếng việt') ?>
					  	</div>
					  	<div class="content_footer_en">
					  	<?= $form->field($model, 'contentshortEN')->textInput([
					  		'rows' => '12',
					  		'id' => 'content_footer_en',
					  		'value' => !empty(json_decode($data['short_description'])->en) ? json_decode($data['short_description'])->en : " "
					  	])->label('Ngôn ngữ tiếng anh') ?>
					  	</div>
					</div>
					<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
					<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Loại Visa
					</label>
					<div class="col-sm-8">
  						<?= $form->field($model, 'typeProduct')
					        ->dropDownList(
					            $map,
					            ['options' => [$data['id_type_product'] => ['Selected'=>'selected']]]
					        )->label(false); ?>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Visa
					</label>
					<div class="col-sm-8">
						<?php //print_r($data) ?>
  						<?= $form->field($model, 'subTypeProduct')->widget(DepDrop::classname(), [
						    // 'options' => [$data['id_sub_type_product'] => ['Selected'=>'selected']],
						    'options' => ['id'=>'subtypeproduct'],
						    'pluginOptions'=>[
						        'depends'=>['productform-typeproduct'],
						        'initialize'=>true,
						        'url' => Url::to(['product/get-sub-type-product' , 'id' => $data['id_sub_type_product']])
						    ]
						 ])->label(false);?>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Country
					</label>
					<div class="col-sm-8">
						<?php //print_r($data) ?>
  						<?= $form->field($model, 'countryProduct')->widget(DepDrop::classname(), [
						    // 'options' => [$data['id_sub_type_product'] => ['Selected'=>'selected']],
						    'options' => ['id'=>'coutryproduct'],
						    'pluginOptions'=>[
						        'depends'=>['subtypeproduct'],
						        'initialize'=>true,
						        'url' => Url::to(['product/get-country-product' , 'id' => $data['id_country_product']])
						    ]
						 ])->label(false);?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Hình Ảnh
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'image')->fileInput([
  							'class' => 'image',
  							'accept'=>".png,.jpg",
  							'value' => Url::to([$data['image']])
  						])->label(false) ?>
					  	<div class="display_image">
					  		<img src="<?php echo Url::to('../../'.$data['image'])  ?>" width=200px>
					  	</div>
					  	<div>Hình Ảnh (370x240)</div>
					</div>
				</div>
				<div class="form-group row">
					<label  class="col-sm-2 control-label">
						Status
					</label>
					<div class="col-sm-10">
						<?php ($data['status'] == 1) ? $check = true : $check = false ?>
					  	<?= $form
                        ->field($model, 'status')
                        ->checkbox([
                        	"data-toggle" => "toggle", 
                        	"data-size"=>"sm",
                        	"checked"=>$check
                        ])->label(false)?>
					</div>
				</div>
		  		<div class="form-group row">
		  			<div class="col-sm-2"></div>
			  		<div class="col-sm-2">
					 	<button type="submit" class="btn btn-info ">Cập Nhật</button>
					</div>
			  	</div>
	 		</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
