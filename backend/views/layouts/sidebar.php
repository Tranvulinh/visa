<?php
use yii\helpers\Url;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">BACKEND SETTING</li>
          <!-- SLider -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Banner</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="slider"><a href="<?php echo Url::to(['/slider']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Banner</a></li>
              <li data-type="add-slider"><a href="<?php echo Url::to(['/slider/add-slider']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới Banner</a></li>
            </ul>
          </li>
          <!-- END SLider -->
          <!-- Loại Visa -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Menu Root Visa</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="type-product"><a href="<?php echo Url::to(['/type-product']);  ?>"><i class="fa fa-circle-o"></i> Danh sách Menu Root Visa</a></li>
              <li data-type="add-type-product"><a href="<?php echo Url::to(['/type-product/add-type-product']);  ?>"><i class="fa fa-circle-o"></i>Thêm mới Menu Root Visa</a></li>
            </ul>
          </li>
          <!-- END Loại Visa -->
          <!-- Visa -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Dịch vụ </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="sub-type-product"><a href="<?php echo Url::to(['/sub-type-product']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Dịch vụ </a></li>
              <li data-type="add-sub-type-product"><a href="<?php echo Url::to(['/sub-type-product/add-sub-type-product']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới Dịch vụ </a></li>
            </ul>
          </li>
          <!-- END Loại Visa -->
          <!-- Country Visa -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span> Quốc gia</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="country-product"><a href="<?php echo Url::to(['/country-product']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Quốc gia</a></li>
              <li data-type="add-country-product"><a href="<?php echo Url::to(['/country-product/add-country-product']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới Quốc gia</a></li>
            </ul>
          </li>
          <!-- END Country Visa -->
          <!-- Bài viết Visa -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Bài Viết Visa</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="product"><a href="<?php echo Url::to(['/product']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Bài Viết Visa</a></li>
              <li data-type="add-product"><a href="<?php echo Url::to(['/product/add-product']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới Bài Viết Visa</a></li>
            </ul>
          </li>
          <!-- END Bài viết Visa -->
          <!-- Loại Post -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Thư mục tin tức</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="type-post"><a href="<?php echo Url::to(['/type-post']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Thư mục tin tức</a></li>
              <li data-type="add-type-post"><a href="<?php echo Url::to(['/type-post/add-type-post']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới Thư mục tin tức</a></li>
            </ul>
          </li>
          <!-- END Loại Post -->
          <!-- Post -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Bài viết tin tức</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="post"><a href="<?php echo Url::to(['/post']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách bài viết tin tức</a></li>
              <li data-type="add-post"><a href="<?php echo Url::to(['/post/add-post']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới bài viết tin tức</a></li>
            </ul>
          </li>
          <!-- END Loại Post -->
          <!-- Consultant -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Nhà Tư Vấn</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="consultant"><a href="<?php echo Url::to(['/consultant']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách Nhà Tư Vấn</a></li>
              <li data-type="add-consultant"><a href="<?php echo Url::to(['/consultant/add-consultant']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới</a></li>
            </ul>
          </li>
          <!-- END Loại Post -->
          <!-- Consultant -->
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Cảm nhận khách hàng</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li data-type="custome-fell"><a href="<?php echo Url::to(['/custome-fell']);  ?>"><i class="fa fa-circle-o"></i> Danh Sách</a></li>
              <li data-type="add-custome-fell"><a href="<?php echo Url::to(['/custome-fell/add-custome-fell']);  ?>"><i class="fa fa-circle-o"></i>Thêm Mới</a></li>
            </ul>
          </li>
          <!-- END Loại Post -->
          <!-- Setting -->
          <li data-type="settings">
            <a href="<?php echo Url::to(['/setting']);  ?>">
              <i class="fa fa-gears"></i><span>Setting</span>
            </a>
          </li>
          <li data-type="info-mail">
            <a href="<?php echo Url::to(['/info-mail']);  ?>">
              <i class="fa fa-gears"></i><span>Thông Tin Khách Hàng</span>
            </a>
          </li>
         <!--  <li data-type="settings">
            <a href="<?php echo Url::to(['site/signup']);  ?>">
              <i class="fa fa-gears"></i><span>Tạo tài khoản</span>
            </a>
          </li> -->
        </ul>
    </section>
</aside>