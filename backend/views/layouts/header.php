<?php
use yii\helpers\Url;
?>
<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <span class="logo-lg"><b>Quản trị</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!-- <span class="sr-only">Toggle navigation</span> -->
      <?php if(!Yii::$app->user->isGuest) : ?>
      <a href="<?php echo Url::to('site/logout') ?>" style="color:white"><span class="pull-right" style="padding: 10px;">Đăng Xuất</span></a>
      <?php endif ?>
      </nav>
  </header>
