<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
// use dosamigos\tinymce\TinyMce;
// use yii\web\JsExpression;
?>
<?php //echo json_decode($setting['content_footer'])->vi;die(); ?>
<style>
	.has-error .help-block-error{
		transition:height 0.3s ease-out; 
		height: 15px;
		font-style: italic;
    	font-weight: bold;
	}
	.help-block-error{		 
		 height:0px;
		 transition:height 0.3s ease-out;
	}
	input {
		border:1px solid !important;
	}
	label {
    	font-weight: 400;
    	color: black;
	}
	.form-control:focus{
		border:2px solid !important;
		border-color: #3c8dbc !important;
    	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, 0.6);
	}
	.box-body{
		padding:25px;
	}
	.hidden{
		transition: 0.5s;
	}
	.show{
		transition: 0.5s;
	}
</style>
<div class="box box-primary">
	<?php  echo Yii::$app->controller->renderPartial('/layouts/breadcum', ['title' => $title])  ?>
	<div class="row">
		<div class="col-md-12">
		 	<?php if (Yii::$app->session->hasFlash('success')): ?>
			    <div class="alert alert-success alert-dismissable"> 
			        <i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('success') ?>
			    </div>
			<?php endif; ?> 
			<?php if (Yii::$app->session->hasFlash('error')): ?>
			    <div class="alert alert-error alert-dismissable"> 
			        <?= Yii::$app->session->getFlash('error') ?>
			    </div>
			<?php endif; ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    		<?php $form = ActiveForm::begin(
			[
				'id' => 'form-setting',
				'options' => [
			        'enctype' => 'multipart/form-data'
			    ]
			]);?>
			<div class="box-body">
		  		<div class="form-group row">
		  			<?= $form->field($model, 'id')->hiddenInput(['value'=>$setting['id']])->label(false); ?>
		  			<label class="col-sm-2 col-form-label">
						Email
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'email')->textInput(['autofocus' => true,'value' => $setting['email']])->label(false) ?>
					</div>
					<label class="col-sm-2 col-form-label">
						Địa Chỉ
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'address')->textInput(['value' => $setting['address']])->label(false) ?>
					</div>
		  		</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Điện Thoại Bàn
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'phone')->textInput(['value' => $setting['phone']])->label(false) ?>
					</div>
					<label class="col-sm-2 col-form-label">
						Điện Thoại Di Động
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'mobilephone')->textInput(['value' => $setting['mobilephone']])->label(false) ?>
					</div>
		  		</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Giờ Làm Việc
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'workTime')->textInput(['value' => $setting['work_time']])->label(false) ?>
					</div>
					<label class="col-sm-2 col-form-label">
						Link Map
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkMap')->textInput(['value' => $setting['link_map']])->label(false) ?>
					</div>
		  		</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Link Skype
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkSkype')->textInput(['value' => $setting['link_skype']])->label(false) ?>
					</div>
					<label for="link_viber" class="col-sm-2 col-form-label">
						Link Viber
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkViber')->textInput(['value' => $setting['link_viber']])->label(false) ?>
					</div>
		  		</div>
		  		<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Link Printerest
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkPrinterest')->textInput(['value' => $setting['link_printerest']])->label(false) ?>
					</div>
					<label class="col-sm-2 col-form-label">
						Link Instagram
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkInstagram')->textInput(['value' => $setting['link_instagram']])->label(false) ?>
					</div>
		  		</div>
		  		<div class="form-group row">
		  			<label for="link_twitter" class="col-sm-2 col-form-label">
						Link Twitter
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkTwitter')->textInput(['value' => $setting['link_twitter']])->label(false) ?>
					</div>
					<label for="link_youtube" class="col-sm-2 col-form-label">
						Link Facebook
					</label>
					<div class="col-sm-4">
					  	<?= $form->field($model, 'linkFacebook')->textInput(['value' => $setting['link_facebook']])->label(false) ?>
					</div>
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Content Footer </div>
							<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentFooterVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi',
						  		'value' => !empty(json_decode($setting['content_footer'])->vi) ? json_decode($setting['content_footer'])->vi : " "
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentFooterEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en',
						  		'value' => !empty(json_decode($setting['content_footer'])->en) ? json_decode($setting['content_footer'])->en : " "
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<div class="form-group row">
			  			<label class="col-sm-2 col-form-label">
							<div>Content Liên Hệ </div>
							<a class="btn btn-info change" style="margin-top: 10px">Thay đổi ngôn ngữ</a>
						</label>
						<div class="col-sm-10">
							<div class="content_footer_vi">
						  	<?= $form->field($model, 'contentContactVI')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_vi',
						  		'value' => !empty(json_decode($setting['content_contact'])->vi) ? json_decode($setting['content_contact'])->vi : " "
						  	])->label('Ngôn ngữ tiếng việt') ?>
						  	</div>
						  	<div class="content_footer_en">
						  	<?= $form->field($model, 'contentContactEN')->textarea([
						  		'rows' => '12',
						  		'id' => 'content_footer_en',
						  		'value' => !empty(json_decode($setting['content_contact'])->en) ? json_decode($setting['content_contact'])->en : " "
						  	])->label('Ngôn ngữ tiếng anh') ?>
						  	</div>
						</div>
						<!-- <div class="col-sm-8 content_footer_en" style="display: none"> -->
						<!-- </div> -->
		  		</div>
		  		<!-- <div class="form-group row">
		  			<label for="link_twitter" class="col-sm-2 col-form-label" >
						Content Footer
					</label>
		  		</div> -->
		  		<div class="form-group row">
		  			<label for="Logo" class="col-sm-2 col-form-label">
						Logo
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'logo')->fileInput(['value' => $setting['logo_image'],'class' => 'logo', 'accept'=>".png,.jpg" ])->label(false) ?>
					  	<div class="display_image">
					  		<img src="<?php echo Url::to("../".$setting['logo_image']) ?>" width=200px>
					  	</div>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Banner Bài Viết
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'bannerPost')->fileInput(['value' => $setting['banner_post'],'class' => 'banner_post', 'accept'=>".png,.jpg" ])->label(false) ?>
					  	<div class="display_image_banner_post">
					  		<img src="<?php echo Url::to("../".$setting['banner_post']) ?>" width=200px>
					  	</div>
					</div>
				</div>
				<div class="form-group row">
		  			<label class="col-sm-2 col-form-label">
						Banner Visa
					</label>
					<div class="col-sm-10">
  						<?= $form->field($model, 'bannerVisa')->fileInput(['value' => $setting['banner_visa'],'class' => 'banner_visa', 'accept'=>".png,.jpg" ])->label(false) ?>
					  	<div class="display_image_banner_visa">
					  		<img src="<?php echo Url::to("../".$setting['banner_visa']) ?>" width=200px>
					  	</div>
					</div>
				</div>
		  		<div class="form-group row">
		  			<div class="col-sm-2"></div>
			  		<div class="col-sm-2">
					 	<button type="submit" class="btn btn-info ">Cập Nhật</button>
					</div>
			  	</div>
	 		</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>