<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        'css/bower_components/bootstrap/dist/css/bootstrap.min.css',
      
        'css/bower_components/font-awesome/css/font-awesome.min.css',
        'css/bower_components/Ionicons/css/ionicons.min.css',
        'css/dist/css/AdminLTE.min.css',
        'css/dist/css/skins/_all-skins.min.css',
        'css/bower_components/morris.js/morris.css',
        'css/bower_components/jvectormap/jquery-jvectormap.css',
        'css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'css/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        'css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        // 'css/plugins/jquery.growl/jquery.growl.css',
        'css/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
        'css/toggle_checkbox.css',
        'css/site.css',
        'css/css.css',
        // 'css/template_css/AdminLTE.min.css',
        // 'css/dist/css/skins/_all-skins.min.css',
        // 'css/template_css/skins/skin-black.min.css',
        // 'css/template_css/skins/skin-black-light.min.css',
        'css/dist/css/skins/skin-blue.min.css',
        // 'css/template_css/skins/skin-blue-light.min.css',
        // 'css/template_css/skins/skin-green-light.min.css',
        // 'css/template_css/skins/skin-purple.min.css',
        // 'css/template_css/skins/skin-purple-light.min.css',
        // 'css/template_css/skins/skin-red.min.css',
        // 'css/template_css/skins/skin-red-light.min.css',
        // 'css/template_css/skins/skin-yellow.min.css',
        // 'css/template_css/skins/skin-yellow-light.min.css',
        // 'css/template_css/alt/AdminLTE-bootstrap-social.min.css',
        // 'css/template_css/alt/AdminLTE-fullcalendar.min.css',
        // 'css/template_css/alt/AdminLTE-select2.min.css',
        // 'css/template_css/alt/AdminLTE-without-plugins.min.css',
    ];
    public $js = [

        // "css/bower_components/jquery/dist/jquery.min.js",

        "css/bower_components/jquery-ui/jquery-ui.min.js",

        "css/bower_components/bootstrap/dist/js/bootstrap.min.js",
        // "css/bower_components/raphael/raphael.min.js",
        // "css/bower_components/morris.js/morris.min.js",
        // "css/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js",
        // "css/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
        // "css/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
        // "css/bower_components/jquery-knob/dist/jquery.knob.min.js",
        "css/bower_components/moment/min/moment.min.js",
        // "css/bower_components/bootstrap-daterangepicker/daterangepicker.js",
        // "css/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
        // "css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
        "css/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
        "css/bower_components/fastclick/lib/fastclick.js",
        "css/dist/js/adminlte.min.js",
        // 'css/plugins/jquery.growl/jquery.growl.js',
        // "css/dist/js/pages/dashboard.js",
        // "css/dist/js/demo.js",
        // 'css/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'css/bower_components/datatables.net/js/jquery.dataTables.min.js',
        'css/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
        'js/toggle_checkbox.js',
        // 'css/bower_components/ckeditor/ckeditor.js',
        
        // 'js/validate_jquery.js',
        // 'js/validate_jquery_addition.js',
        'js/js.js',
        'js/tinymce/tinymce.min.js',
        'js/tinymce/jquery.tinymce.min.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];
}
